<?php
	require_once('authorize.php');
	require_once('includes/config.php');
	require_once('includes/meta.php');
	$sth = $dbh->prepare("SELECT comment FROM comments WHERE commentId = ?");
	$sth->setFetchMode(PDO::FETCH_OBJ);
	$sth->execute(array($_GET['commentId']));
	while($row = $sth->fetch()){
		echo '<form id="comment-form" name="CommentEdit" action="checklistcommentpost.php" method="post">';
			echo '<h2>Update Comment</h2>';
			echo '<textarea id="comment-textarea" name="comment">' . $row->comment . '</textarea>';
			echo '<input type="hidden" name="releaseId" value="' . $_GET['releaseId'] . '" />';
			echo '<input type="hidden" name="tableName" value="' . $_GET['tableName'] . '" />';
			echo '<input type="hidden" name="rowId" value="' . $_GET['rowId'] . '" />';
			echo '<input type="hidden" name="commentId" value="' . $_GET['commentId'] . '" />';
			echo '<br />';
			echo '<input id="edit-comment" class="input-btn" type="submit" name="CommentEdit" value="Update" />';
		echo '</form>';
	}
	require_once('includes/closeconn.php');
?>