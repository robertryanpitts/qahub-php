<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	$editedDate = date('Y-m-d  H:i:s');
	$sql = "UPDATE " . str_replace('-', '', $_GET['table']) . " SET value = ?, editedBy = ?, editedDate = ? WHERE releaseId = ? AND rowId = ?";
	$sth = $dbh->prepare($sql);
	$sth->execute(array($_GET['value'], $_COOKIE['user'], $editedDate, $_GET['releaseId'], $_GET['rowId']));
	$sth = $dbh->prepare("UPDATE releases SET modifiedBy = ?, editedDate = ? WHERE id = ?");
	$sth->execute(array($_COOKIE['user'], $editedDate, $_GET['rowId']));
	require_once('includes/closeconn.php');
	$arr = array ('table' => $_GET['table'], 'rowId' => $_GET['rowId'], 'editedDate' => date('F j, Y @ g:ia', strtotime($editedDate)), 'editedBy' => $_COOKIE['user']);
	echo json_encode($arr);
?>