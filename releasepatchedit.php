<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	$sth = $dbh->prepare("SELECT * FROM releases WHERE id = ? LIMIT 1");
	$sth->setFetchMode(PDO::FETCH_OBJ);
	$sth->execute(array($_GET['releaseId']));
	while($row = $sth->fetch()){
		$releaseName = $row->name;
	}
	$sth = $dbh->prepare("SELECT * FROM releasespatches WHERE itemId = ? LIMIT 1");
	$sth->setFetchMode(PDO::FETCH_OBJ);
	$sth->execute(array($_GET['itemId']));
	while($row = $sth->fetch()){
		$type = $row->type;
		$referenceType = $row->referenceType;
		$referenceId = $row->referenceId;
		$title = htmlspecialchars($row->title);
		$dataChange = $row->dataChange;
		$configChange = $row->configChange;
		$affectedProduct = $row->affectedProduct;
		$affectedBrowsers = $row->affectedBrowsers;
		$affectedDesigns = $row->affectedDesigns;
		$comfortLevel = $row->comfortLevel;
		$riskLevel = $row->riskLevel;
		$comments = htmlspecialchars($row->comments);
	}
	$sth = $dbh->prepare("SELECT * FROM affectedbrowsers WHERE itemId = ? AND type = ?");
	$sth->setFetchMode(PDO::FETCH_OBJ);
	$sth->execute(array($_GET['itemId'], $type));
	$affectedBrowsers = array();
	while($row = $sth->fetch()){
		$affectedBrowsers[] = $row->label;
	}
	$sth = $dbh->prepare("SELECT * FROM affecteddesigns WHERE itemId = ? AND type = ?");
	$sth->setFetchMode(PDO::FETCH_OBJ);
	$sth->execute(array($_GET['itemId'], $type));
	$affectedDesigns = array();
	while($row = $sth->fetch()){
		$affectedDesigns[] = $row->label;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Edit <?php echo $type; ?>: <?php echo $title; ?></title>
	<?php
		require_once('includes/meta.php');
	?>
	<script type="text/javascript">
		$(document).ready(function(){
			checkCheckboxes();
		});
		var checkCheckboxes = function checkCheckboxes(){
			$('input:checked').parent().addClass('checkedBox');
			$('input:not(:checked)').parent().removeClass('checkedBox');
		}
	</script>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<h1>
					<strong>Edit <?php echo $type; ?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="releaseitems.php?releaseId=<?php echo $_GET['releaseId']; ?>" style="font-size:18px;">Back to the Release List</a>
				</h1>
				<br />
				<form name="add_item" method="post" action="releasepatchpostedit.php">
					<div class="form-row">
						Title:
						<br />
						<select name="type">
							<option value="Data" <?php echo ($type == 'Data') ? 'selected="selected"' : ''; ?>>Data</option>
							<option value="Patch" <?php echo ($type == 'Patch') ? 'selected="selected"' : ''; ?>>Patch</option>
							<option value="Project" <?php echo ($type == 'Project') ? 'selected="selected"' : ''; ?>>Project</option>
							<option value="Upgrade" <?php echo ($type == 'Upgrade') ? 'selected="selected"' : ''; ?>>Upgrade</option>
						</select>
					<br /><br />
					<div class="form-row">
						Reference:
						<br />
						<span class="tooltip">
							<img src="images/tooltip.png" /> leave ID # field blank if not applicable
						</span>
						<br />
						<select name="referenceType">
							<option value="FogBugz" <?php echo ($referenceType == 'FogBugz') ? 'selected="selected"' : ''; ?>>FogBugz</option>
							<option value="VersionOne" <?php echo ($referenceType == 'VersionOne') ? 'selected="selected"' : ''; ?>>VersionOne</option>
							<option value="ZenDesk" <?php echo ($referenceType == 'ZenDesk') ? 'selected="selected"' : ''; ?>>ZenDesk</option>
						</select><span class="form-row">&nbsp;&nbsp;ID #</span>
						<input type="text" name="referenceId" maxlength="100" value="<?php echo $referenceId; ?>" />
					</div>
					<div class="form-row">
						Title:
						<br />
						<input class="large-input" type="text" name="title" maxlength="300" value="<?php echo $title; ?>" />
					</div>
					<div class="form-row">
						Data Change?
						<br />
						<input type='radio' name='dataChange' value='1' <?php echo ($dataChange == '1') ? 'checked="checked"' : ''; ?> />
						<strong>Yes</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='radio' name='dataChange' value='0' <?php echo ($dataChange == '0') ? 'checked="checked"' : ''; ?> />
						<strong>No</strong>
					</div>
					<div class="form-row">
						Configuration File Change?
						<br />
						<input type='radio' name='configChange' value='1' <?php echo ($configChange == '1') ? 'checked="checked"' : ''; ?> />
						<strong>Yes</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='radio' name='configChange' value='0' <?php echo ($configChange == '0') ? 'checked="checked"' : ''; ?> />
						<strong>No</strong>
					</div>
					<div class="form-row">
						Affected Browsers:
						<br />
						<span class="tooltip">
							<img src="images/tooltip.png" /> check all that apply
						</span>
						<br />
						<table id="affedcted-browsers-grid">
							<tr>
								<?php
									$sth = $dbh->prepare("SELECT * FROM affectedbrowsersdata WHERE active = true ORDER BY label ASC");
									$sth->setFetchMode(PDO::FETCH_OBJ);
									$sth->execute();
									$i = 1;
									while($row = $sth->fetch()){
										if($i == 1){
											echo '<td valign="top">';
												echo '<label><input type="checkbox" name="affectedbrowsers[]" value="' . $row->browser . '"';
													echo (in_array($row->browser, $affectedBrowsers)) ? 'checked="checked"' : '';
												echo ' onclick="checkCheckboxes();" />' . $row->label . '</label>';
												echo '<br />';
											$i = $i+1;
										}else if($i == 3){
												echo '<label><input type="checkbox" name="affectedbrowsers[]" value="' . $row->browser . '"';
													echo (in_array($row->browser, $affectedBrowsers)) ? 'checked="checked"' : '';
												echo ' onclick="checkCheckboxes();" />' . $row->label . '</label>';
											echo '</td>';
											$i = 1;
										}else{
											echo '<label><input type="checkbox" name="affectedbrowsers[]" value="' . $row->browser . '"';
												echo (in_array($row->browser, $affectedBrowsers)) ? 'checked="checked"' : '';
											echo ' onclick="checkCheckboxes();" />' . $row->label . '</label>';
											echo '<br />';
											$i = $i+1;
										}
									}
								?>
							</tr>
						</table>
					</div>
					<div class="form-row">
						Affected Designs:
						<br />
						<span class="tooltip">
							<img src="images/tooltip.png" /> check all that apply
						</span>
						<br />
						<table id="affected-designs-grid" width="100%">
							<tr>
								<?php
									$sth = $dbh->prepare("SELECT * FROM affecteddesignsdata WHERE active = true ORDER BY label ASC");
									$sth->setFetchMode(PDO::FETCH_OBJ);
									$sth->execute();
									$i = 1;
									while($row = $sth->fetch()){
										if($i == 1){
											echo '<td valign="top">';
												echo '<label><input type="checkbox" name="affecteddesigns[]" value="' . $row->design . '"';
													echo (in_array($row->design, $affectedDesigns)) ? 'checked="checked"' : '';
												echo ' onclick="checkCheckboxes();" />' . $row->label . '</label>';
												echo '<br />';
											$i = $i+1;
										}else if($i == 15){
												echo '<label><input type="checkbox" name="affecteddesigns[]" value="' . $row->design . '"';
													echo (in_array($row->design, $affectedDesigns)) ? 'checked="checked"' : '';
												echo ' onclick="checkCheckboxes();" />' . $row->label . '</label>';
											echo '</td>';
											$i = 1;
										}else{
											echo '<label><input type="checkbox" name="affecteddesigns[]" value="' . $row->design . '"';
												echo (in_array($row->design, $affectedDesigns)) ? 'checked="checked"' : '';
											echo ' onclick="checkCheckboxes();" />' . $row->label . '</label>';
											echo '<br />';
											$i = $i+1;
										}
									}
								?>
							</tr>
						</table>
					</div>
					<div class="form-row">
						Affected Product:
						<br />
						<select name="affectedProduct">
							<option value="N/A" <?php echo ($affectedProduct == 'N/A') ? 'selected="selected"' : ''; ?>>N/A</option>
							<option value="Asset Server" <?php echo ($affectedProduct == 'Asset Server') ? 'selected="selected"' : ''; ?>>Asset Server</option>
							<option value="Fastlane" <?php echo ($affectedProduct == 'FastLane') ? 'selected="selected"' : ''; ?>>FastLane</option>
							<option value="Fusion Website" <?php echo ($affectedProduct == 'Fusion Website') ? 'selected="selected"' : ''; ?>>Fusion Website</option>
							<option value="Inventory Process" <?php echo ($affectedProduct == 'Inventory Process') ? 'selected="selected"' : ''; ?>>Inventory Process</option>
							<option value="New Relic" <?php echo ($affectedProduct == 'New Relic') ? 'selected="selected"' : ''; ?>>New Relic</option>
						</select>
					</div>
					<div class="form-row">
						Comfort Level:
						<br />
						<select name="comfortLevel">
							<option value="0"></option>
							<option value="1" class="release-level-bad" <?php echo ($comfortLevel == '1') ? 'selected="selected"' : ''; ?>>Low</option>
							<option value="2" class="release-level-mediocre" <?php echo ($comfortLevel == '2') ? 'selected="selected"' : ''; ?>>Medium</option>
							<option value="3" class="release-level-good" <?php echo ($comfortLevel == '3') ? 'selected="selected"' : ''; ?>>High</option>
						</select>
					</div>
					<div class="form-row">
						Risk Level:
						<br />
						<select name="riskLevel">
							<option value="0"></option>
							<option value="1" class="release-level-good" <?php echo ($riskLevel == '1') ? 'selected="selected"' : ''; ?>>Low</option>
							<option value="2" class="release-level-mediocre" <?php echo ($riskLevel == '2') ? 'selected="selected"' : ''; ?>>Medium</option>
							<option value="3" class="release-level-bad" <?php echo ($riskLevel == '3') ? 'selected="selected"' : ''; ?>>High</option>
						</select>
					</div>
					<div class="form-row">
						Comments:
						<br />
						<input class="large-input" type="text" name="comments" value="<?php echo $comments; ?>" />
					</div>
					<div class="form-row">
						<input name="releaseId" type="hidden" value="<?php echo $_GET['releaseId']; ?>" />
						<input name="itemId" type="hidden" value="<?php echo $_GET['itemId']; ?>" />
						<input name="editedBy" type="hidden" value="<?php echo $_COOKIE['user']; ?>" />
						<input name="editedDate" type="hidden" value="<?php echo date("Y-m-d  H:i:s"); ?>" />
						<input class="input-btn" type="submit" name="edit" value="Update" />
						<input class="input-btn btn-cancel" type="button" name="cancel" value="Cancel" onclick="history.go(-1)" />
					</div>
				</form>
				<br />
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>