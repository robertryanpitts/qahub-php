<?php
	require_once('includes/config.php');
	require_once('includes/config.php');
	if(isset($_POST['add'])){
		$sth = $dbh->prepare("INSERT INTO releasespatches (releaseId, type, referenceType, referenceId, title, dataChange, configChange, affectedProduct, comfortLevel, riskLevel, comments, editedBy, editedDate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$sth->execute(array($_POST['releaseId'], $_POST['type'], $_POST['referenceType'], $_POST['referenceId'], $_POST['title'], $_POST['dataChange'], $_POST['configChange'], $_POST['affectedProduct'], $_POST['comfortLevel'], $_POST['riskLevel'], $_POST['comments'], $_COOKIE['user'], date("Y-m-d  H:i:s")));
		$itemId = $dbh->lastInsertId();
		foreach($_POST['affectedbrowsers'] as $affectedBrowsers){
			$sth = $dbh->prepare("INSERT INTO affectedbrowsers (releaseId, itemId, type, label, editedBy, editedDate) VALUES (?, ?, ?, ?, ?, ?)");
			$sth->execute(array($_POST['releaseId'], $itemId, $_POST['type'], $affectedBrowsers, $_COOKIE['user'], date("Y-m-d  H:i:s")));
		}
		foreach($_POST['affecteddesigns'] as $affectedDesigns){
			$sth = $dbh->prepare("INSERT INTO affecteddesigns (releaseId, itemId, type, label, editedBy, editedDate) VALUES (?, ?, ?, ?, ?, ?)");
			$sth->execute(array($_POST['releaseId'], $itemId, $_POST['type'], $affectedDesigns, $_COOKIE['user'], date("Y-m-d  H:i:s")));
		}
	}
	require_once('includes/closeconn.php');
	header('location:releaseitems.php?releaseId=' . $_POST['releaseId'] . '&type=' . $_POST['type'] . '&addItem=success');
?>