<?php
	require_once('includes/config.php');
	require_once('includes/config.php');
	$sth = $dbh->prepare("DELETE FROM releasespatches WHERE itemId = ?");
	$sth->execute(array($_GET['itemId']));
	$sth = $dbh->prepare("DELETE FROM affectedbrowsers WHERE itemId = ? AND type = ?");
	$sth->execute(array($_GET['itemId'], $_GET['type']));
	$sth = $dbh->prepare("DELETE FROM affecteddesigns WHERE itemId = ? AND type = ?");
	$sth->execute(array($_GET['itemId'], $_GET['type']));
	require_once('includes/closeconn.php');
	header('location:releaseitems.php?releaseId=' . $_GET['releaseId'] . '&type=' . $_GET['type'] . '&deleteReleasePatch=success');
?>