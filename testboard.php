<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Test Board</title>
	<?php
		require_once('includes/meta.php');
	?>
	<script src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		var VisitedHistory = [];

		var CM =
				{
					"Feature": "{customerid}.{feature}.clickmotivefusiondev.com{suffix}",
					"Staging": "{customerid}.clickmotivefusionstage.com{suffix}",
					"Deploy": "{customerid}.deploy.clickmotivefusion.com{suffix}",
					"Production": "{customerid}.clickmotivefusion.com{suffix}"
				};

		var FT =
				{
					"Feature": "fusiontools.{feature}.clickmotivedev.com",
					"Staging": "fusiontools.clickmotivestage.com",
					"Production": "fusiontools.clickmotive.com"
				};

		var FA =
				{
					"Feature": "intranet.{feature}.clickmotivedev.com/admin/Customer.aspx?CustomerId={customerid}",
					"Staging": "intranet.clickmotivestage.com/admin/Customer.aspx?CustomerId={customerid}",
					"Production": "intranet.clickmotive.com/admin/Customer.aspx?CustomerId={customerid}"
				};

		var Suffix =
				{
					"Clear": "/xmlcache/cachecontrol.aspx/remove/{customerid}"
				};

		$(document).ready(function () {
			if (supports_html5_storage()) {
				loadSavedDataIntoTextBox('Feature', '#txt-feature');
				loadSavedDataIntoTextBox('CustomerId', '#txt-customer-id');
			}
		});
		var goToUrl = function goToUrl(baseurl) {
			baseurl = replaceKey(baseurl, '{suffix}', 'hfSuffix');
			baseurl = replaceKey(baseurl, '{feature}', 'txtFeature');


			var allCustomerIds = $('#txt-customer-id').val().split(',');

			for (var _i = 0; _i < allCustomerIds.length; _i++) {
				gotopage(replaceWithString(baseurl, '{customerid}', allCustomerIds[_i]));
			}
		}

		var gotopage = function gotopage(baseurl) {
			VisitedHistory.push({ URL: 'http://' + baseurl });
			updateHistory();

			if (supports_html5_storage()) {
				SaveDataFromTextBox('Feature', '#txt-feature');
				SaveDataFromTextBox('CustomerId', '#txt-customer-id');
			}

			window.open('http://' + baseurl);
		}

		var goToUrlWithSuffix = function goToUrlWithSuffix(baseurl, suffix) {
			$('#hf-suffix').val(suffix);
			goToUrl(baseurl + '{suffix}');
		}

		var replaceKey = function replaceKey(baseurl, key, source) {
			var tempBaseUrl = baseurl;


			while (tempBaseUrl.indexOf(key) > -1) {
				var keyvalue = document.getElementById(source).value;
				tempBaseUrl = tempBaseUrl.replace(key, keyvalue);
			}

			return tempBaseUrl;
		}

		var replaceWithString = function replaceWithString(baseurl, key, stringvalue) {
			var tempBaseUrl = baseurl;


			while (tempBaseUrl.indexOf(key) > -1) {
				tempBaseUrl = tempBaseUrl.replace(key, stringvalue);
			}

			return tempBaseUrl;
		}

		var updateHistory = function updateHistory() {
			$("#history-link-list").empty();
			$("#history-list").tmpl(VisitedHistory).appendTo("#history-link-list");

		}

		var supports_html5_storage = function supports_html5_storage() // via http://diveintohtml5.org/storage.html
		{
			try {
				return 'localStorage' in window && window['localStorage'] !== null;
			} catch (e) {
				return false;
			}
		}

		var loadSavedDataIntoTextBox = function loadSavedDataIntoTextBox(key, textbox) {
			var tempString = localStorage[key];
			if (tempString) {
				$(textbox).val(tempString);
			}
		}

		var SaveDataFromTextBox = function SaveDataFromTextBox(key, textbox) {
			var tempString = $(textbox).val();
			localStorage[key] = tempString;
		}
	</script>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<h1>Test Board</h1>
				<div id="container">
					<div id="link-container" class="left">
						<div class="clear"></div>
						<h2>How to use the testboard:</h2>
						<div class="indent">
							The test board is a great tool to do some quick testing.  It generates many of the most commonly used links based on the criteria that you feed it. Follow the step-by-step instructions below to make the most out of this tool.
							<br /><br />
							<strong>Instructions:</strong>
							<div class="indent">
								<ul>
									<li>First, enter a feature branch or release branch name in to the "Current Feature / Release" field. (if applicable)</li>
									<li>Next, enter the Customer Id(s) you would like to use. Separate Id's by commas. (optional)</li>
									<li>Instead of manually entering the Customer Id(s) you can select the customers you would like to use in the "Test Customers" list.  You can use Shift or Ctrl to select multiple customers.</li>
									<li>Lastly, you optionally enter in a suffix for the URLs. (ex, Service.aspx or Plano/For-Sale/New/)</li>
								</ul>
							</div>
							<strong>Notes:</strong>
							<div class="indent">
								<ul>
									<li>The "Generated Site Links" section generates links to the respective locations based on the information in the 4 data fields.  Clicking those links opens a new page to that location.</li>
									<li>You can see all of your recent URLs visited from this tool in the "History" section.</li>
									<li>You can clear the "History" section by using the "clear history" link or by just refreshing the page.</li>
								</ul>
							</div>
						</div>
						<div id="test-board" class="left">
							<h2>Site Links Data:</h2>
							<form action="#">
								<strong>Current Feature / Release:</strong>
								<br />
								<?php
									$sth = $dbh->prepare("SELECT name FROM releases ORDER BY id DESC LIMIT 1");
									$sth->setFetchMode(PDO::FETCH_OBJ);
									$sth->execute();
									while($row = $sth->fetch()){
										echo '<input class="large-input" id="txt-feature" type="text" value="' . strtolower(str_replace(' ', '-', $row->name)) . '" />';
									}
								?>
								<br /><br />
								<strong>Customer Id(s):</strong>
								<br />
								<input class="large-input" id="txt-customer-id" type="text" value="27804" />
								<br /><br />
								<strong>Test Customers:</strong>
								<br />
								<select id="customer-options" multiple="multiple" size="20" onclick="document.getElementById('txt-customer-id').value = $('#customer-options').val()" onchange="document.getElementById('txt-customer-id').value = $('#customer-options').val()">
									<optgroup label="Acura Family">
										<option value="27804">IT_Test_AcuraExtreme</option>
										<option value="27805">IT_Test_AcuraLightLegacy</option>
										<option value="27806">IT_Test_AcuraDarkLegacy</option>
									</optgroup>
									<optgroup label="Asbury Family">
										<option value="27807">IT_Test_AsburyExtreme</option>
										<option value="27810">IT_Test_NalleyPortal</option>
									</optgroup>
									<optgroup label="AutoGroup Family">
										<option value="27811">IT_Test_AdAssociationAcura</option>
										<option value="27812">IT_Test_AutoGroupPortal</option>
									</optgroup>
									<optgroup label="AutoNation Family">
										<option value="27813">IT_Test_AutoNationExtreme</option>
									</optgroup>
									<optgroup label="BMW Family">
										<option value="27817">IT_Test_BMWExtreme</option>
									</optgroup>
									<optgroup label="Ford Family">
										<option value="28824">IT_Test_CarbonFiber</option>
										<option value="27818">IT_Test_FordLight</option>
										<option value="27819">IT_Test_FordElite</option>
										<option value="27916">IT_Test_FordEliteClassic</option>
										<option value="xxxxx">IT_Test_FordEliteCobaltBlue</option>
										<option value="28825">IT_Test_FordEliteElectricBlue</option>
										<option value="xxxxx">IT_Test_FordEliteMidnightBlue</option>
										<option value="28826">IT_Test_FordFastOne</option>
										<option value="28827">IT_Test_RadiantBlue</option>
										<option value="28829">IT_Test_RadiantSilver</option>
									</optgroup>
									<optgroup label="Dealer Family">
										<option value="28830">IT_Test_DealerElite</option>
										<option value="27822">IT_Test_DealerExtreme</option>
										<option value="27823">IT_Test_DealerLegacyLight</option>
										<option value="28921">IT_Test_FusionLuxe</option>
										<option value="29025">IT_Test_Microsite</option>
									</optgroup>
									<optgroup label="GSM Family">
										<option value="28832">IT_Test_GSMElite</option>
										<option value="27826">IT_Test_GSMExtreme</option>
										<option value="27827">IT_Test_GSMLegacyLight</option>
										<option value="29824">IT_Test_ScionExtreme</option>
										<option value="32789">IT_Test_ScionVerticalNav</option>
									</optgroup>
									<optgroup label="Lincoln Family">
										<option value="33440">IT_Test_LincolnBasic</option>
										<option value="33441">IT_Test_LincolnCarbonFiber</option>
										<option value="33983">IT_Test_LincolnElegance</option>
										<option value="33442">IT_Test_LincolnElite</option>
									</optgroup>
									<optgroup label="Mobile Family">
										<option value="27831">Mobile1 / MobileTouch</option>
										<option value="27832">Mobile1Hub / MobileTouchHub</option>
									</optgroup>
									<optgroup label="Nissan Family">
										<option value="28833">IT_Test_NissanExtreme</option>
									</optgroup>
									<optgroup label="Other Customers">
										<option value="27902">IT_Test_GenericShort</option>
										<option value="7903">IT_Test_GenericLong</option>
									</optgroup>
								</select>
								<br /><br />
								<strong>Suffix:</strong> (e.g., /Plano/For-Sale/New/, /Accessories.aspx, etc...)
								<br />
								<input class="large-input" type="text" id="hf-suffix" />
							</form>
						</div>
						<div id="generated-links" class="left">
							<h2>Generated Site Links</h2>
							<ul class="no-style">
								<li>
									<strong>ClickMotive Fusion</strong>
									<ul class="no-style link-list">
										<li><a href="#" onClick="goToUrl(CM.Feature)">Feature / Release</a> :: <a href="#" onclick="goToUrlWithSuffix(CM.Feature, Suffix.Clear); ">Clear Cache</a> </li>
										<li><a href="#" onClick="goToUrl(CM.Staging)">Staging</a> :: <a href="#" onClick="goToUrlWithSuffix(CM.Staging, Suffix.Clear); ">Clear Cache</a></a></li>
										<li><a href="#" onClick="goToUrl(CM.Deploy)">Deploy</a> :: <a href="#" onClick="goToUrlWithSuffix(CM.Deploy, Suffix.Clear); ">Clear Cache</a></a></li>
										<li><a href="#" onClick="goToUrl(CM.Production)">Production</a> :: <a href="#" onClick="goToUrlWithSuffix(CM.Production, Suffix.Clear); ">Clear Cache</a></a></li>
									</ul>
								</li>
								<li>
									<strong>Fusion Admin</strong>
									<ul class="no-style link-list">
										<li><a href="#" onClick="goToUrl(FT.Feature)">Feature / Release</a></li>
										<li><a href="#" onClick="goToUrl(FT.Staging)">Staging</a></li>
										<li><a href="#" onClick="goToUrl(FT.Production)">Production</a></li>
									</ul>
								</li>
								<li>
									<strong>Admin Tools</strong>
									<ul class="no-style link-list">
										<li><a href="#" onClick="goToUrl(FA.Feature)">Feature / Release</a></li>
										<li><a href="#" onClick="goToUrl(FA.Staging)">Staging</a></li>
										<li><a href="#" onClick="goToUrl(FA.Production)">Production</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
					<div id="history-links" class="left clear">
						<h2>History <span id="clear-history" onClick="VisitedHistory = [];updateHistory();">[<span class="accent">clear history</span>]</span></h2>
						<div id="recently-visited-sites">
							Recently Visited Sites:
							<ul id="history-link-list">
							</ul>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
	<script id="history-list" type="text/x-jquery-tmpl"> 
		<li><a href="${URL}" target="_blank">${URL}</a></li>
	</script>
</body>
</html>