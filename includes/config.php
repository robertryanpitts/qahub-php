<?php
	$dbHost = 'localhost';
	$dbUser = 'USERNAME';
	$dbPass = 'PASSWORD';
	$dbName = 'qahub_dev';
	
	try {
		$dbh = new PDO(sprintf('mysql:host=%s;dbname=%s', $dbHost, $dbName), $dbUser, $dbPass);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
		echo 'ERROR: ' . $e->getMessage();
	}
?>