<?php
	require_once('config.php');
	require_once('../authorize.php');
	
	$keyword = $_POST['partialSearch'];
	$searchType = $_POST['searchType'];
	$searchBy = $_POST['searchBy'];
	
	if($keyword == ''){
		echo '<input type="hidden" id="count" name="count" value="0" />';
		echo '<div class="resultsRowNone">Begin typing to search!</div>';
	}else if($searchType == ''){
		echo '<input type="hidden" id="count" name="count" value="0" />';
		echo '<div class="results-row-none">You must first select a "search type" to search!</div>';
	}else if($searchType == ''){
		echo '<input type="hidden" id="count" name="count" value="0" />';
		echo '<div class="results-row-none">You must first select a "search by" to search!</div>';
	}else if($searchType == 'release-item'){
		if($searchBy == 'affectedDesigns' || $searchBy == 'affectedBrowsers'){
			switch($searchBy){
				case 'affectedDesigns':
					$sth = $dbh->prepare("SELECT releaseId,itemId,type,label FROM affecteddesigns WHERE type = 'Item' AND label LIKE ? ORDER BY releaseId DESC");
					break;
				case 'affectedBrowsers':
					$sth = $dbh->prepare("SELECT releaseId,itemId,type,label FROM affectedbrowsers WHERE type = 'Item' AND label LIKE ? ORDER BY releaseId DESC");
					break;
			}
			$sth->setFetchMode(PDO::FETCH_OBJ);
			$sth->execute('%'.$keyword.'%');
			$count = $sth->rowCount();
			if($count < 1){
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				echo '<div class="results-row-none">There are no results with "' . $keyword . '" in the affected broswers!</div>';
			}else{
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				while($row = $sth->fetch()){
					$releaseId = $row->releaseId;
					$itemId = $row->itemId;
					$sthItems = $dbh->prepare("SELECT itemId,releaseId,title FROM releaseitems WHERE releaseId = ? AND itemId = ? ORDER BY releaseId DESC, itemId ASC");
					$sthItems->setFetchMode(PDO::FETCH_OBJ);
					$sthItems->execute(array($releaseId, $itemId));
					$countItems = $sthItems->rowCount();
					while($rowItems = $sthItems->fetch()){
						$releaseId = $rowItems->releaseId;
						echo '<a href="releaseitems.php?releaseId=' . $releaseId . '">';
							echo '<div class="results-row">';
								echo '<div class="title"><span class="results-title">Title:</span> ' . htmlspecialchars($rowItems->title) . '</div>';
								$sthRelease = $dbh->prepare("SELECT name FROM releases WHERE id = ? ORDER BY id DESC");
								$sthRelease->setFetchMode(PDO::FETCH_OBJ);
								$sthRelease->execute(array($releaseId));
								while($rowRelease = $sthRelease->fetch()){
									echo '<div class="release"><span class="results-title">Release:</span> ' . htmlspecialchars($rowRelease->name) . '</div>';
								}
							echo '</div>';
						echo '</a>';
					}
				}
			}
		}else if($searchBy == 'releaseName'){
			$sth = $dbh->prepare("SELECT p.releaseId,p.title,r.name FROM releaseitems AS p JOIN releases AS r ON r.id = p.releaseId WHERE r.name LIKE ? ORDER BY p.releaseId DESC, i.itemId ASC");
			$sth->setFetchMode(PDO::FETCH_OBJ);
			$sth->execute(array('%'.$keyword.'%'));
			$count = $sth->rowCount();
			if($count < 1){
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				echo '<div class="results-row-none">There are no results with "' . $keyword . '" in the release name!</div>';
			}else{
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				while($row = $sth->fetch()){
					$releaseId = $row->releaseId;
					echo '<a href="releaseitems.php?releaseId=' . $releaseId . '">';
						echo '<div class="results-row">';
							echo '<div class="title"><span class="results-title">Title:</span> ' . htmlspecialchars($row->title) . '</div>';
							echo '<div class="release"><span class="results-title">Release:</span> ' . htmlspecialchars($row->name) . '</div>';
						echo '</div>';
					echo '</a>';
				}
			}
		}else{
			switch($searchBy){
				case 'title':
					$sth = $dbh->prepare("SELECT releaseId,title FROM releaseitems WHERE title LIKE ? ORDER BY releaseId DESC, itemId ASC");
					break;
				case 'referenceId':
					$sth = $dbh->prepare("SELECT releaseId,title FROM releaseitems WHERE referenceId LIKE ? ORDER BY releaseId DESC, itemId ASC");
					break;
				case 'comments':
					$sth = $dbh->prepare("SELECT releaseId,title FROM releaseitems WHERE comments LIKE ? ORDER BY releaseId DESC, itemId ASC");
					break;
			}
			$sth->setFetchMode(PDO::FETCH_OBJ);
			$sth->execute('%'.$keyword.'%');
			$count = $sth->rowCount();
			if($count < 1){
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				echo '<div class="results-row-none">There are no results with "' . $keyword . '" in the ' . $searchBy . '!</div>';
			}else{
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				while($row = $sth->fetch()){
					$releaseId = $row->releaseId;
					echo '<a href="releaseitems.php?releaseId=' . $releaseId . '">';
						echo '<div class="results-row">';
							echo '<div class="title"><span class="results-title">Title:</span> ' . htmlspecialchars($row->title) . '</div>';
							$sthRelease = $dbh->prepare("SELECT name FROM releases WHERE id = ? ORDER BY id DESC");
							$sthRelease->setFetchMode(PDO::FETCH_OBJ);
							$sthRelease->execute(array($releaseId));
							while($rowRelease = $sthRelease->fetch()){
								echo '<div class="release"><span class="results-title">Release:</span> ' . htmlspecialchars($rowRelease->name) . '</div>';
							}
						echo '</div>';
					echo '</a>';
				}
			}
		}
	}else{
		if($searchBy == 'affectedDesigns' || $searchBy == 'affectedBrowsers'){
			switch($searchBy){
				case 'affectedDesigns':
					$sth = $dbh->prepare("SELECT releaseId,itemId,type,label FROM affecteddesigns WHERE type = ? AND label LIKE ? ORDER BY releaseId DESC");
					break;
				case 'affectedBrowsers':
					$sth = $dbh->prepare("SELECT releaseId,itemId,type,label FROM affectedbrowsers WHERE type = ? AND label LIKE ? ORDER BY releaseId DESC");
					break;
			}
			$sth->setFetchMode(PDO::FETCH_OBJ);
			$sth->execute(array(ucfirst($searchType), '%'.$keyword.'%'));
			$count = $sth->rowCount();
			if($count < 1){
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				echo '<div class="results-row-none">There are no results with "' . $keyword . '" in the affected broswers!</div>';
			}else{
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				while($row = $sth->fetch()){
					$releaseId = $row->releaseId;
					$itemId = $row->itemId;
					$sthItems = $dbh->prepare("SELECT itemId,releaseId,title FROM releasespatches WHERE type = ? AND releaseId = ? AND itemId = ? ORDER BY releaseId DESC, itemId ASC");
					$sthItems->setFetchMode(PDO::FETCH_OBJ);
					$sthItems->execute(array(ucfirst($searchType), $releaseId, $itemId));
					$countItems = $sthItems->rowCount();
					while($rowItems = $sthItems->fetch()){
						$releaseId = $rowItems->releaseId;
						echo '<a href="releaseitems.php?releaseId=' . $releaseId . '">';
							echo '<div class="results-row">';
								echo '<div class="title"><span class="results-title">Title:</span> ' . htmlspecialchars($rowItems->title) . '</div>';
								$sthRelease = $dbh->prepare("SELECT name FROM releases WHERE id = ? ORDER BY id DESC");
								$sthRelease->setFetchMode(PDO::FETCH_OBJ);
								$sthRelease->execute(array($releaseId));
								while($rowRelease = $sthRelease->fetch()){
									echo '<div class="release"><span class="results-title">Release:</span> ' . htmlspecialchars($rowRelease->name) . '</div>';
								}
							echo '</div>';
						echo '</a>';
					}
				}
			}
		}else if($searchBy == 'releaseName'){
			$sth = $dbh->prepare("SELECT p.releaseId,p.title,r.name FROM releasespatches AS p JOIN releases AS r ON r.id = p.releaseId WHERE p.type = ? AND r.name LIKE ? ORDER BY p.releaseId DESC, p.itemId ASC");
			$sth->setFetchMode(PDO::FETCH_OBJ);
			$sth->execute(array(ucfirst($searchType), '%'.$keyword.'%'));
			$count = $sth->rowCount();
			if($count < 1){
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				echo '<div class="results-row-none">There are no results with "' . $keyword . '" in the release name!</div>';
			}else{
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				while($row = $sth->fetch()){
					$releaseId = $row->releaseId;
					echo '<a href="releaseitems.php?releaseId=' . $releaseId . '">';
						echo '<div class="results-row">';
							echo '<div class="title"><span class="results-title">Title:</span> ' . htmlspecialchars($row->title) . '</div>';
							echo '<div class="release"><span class="results-title">Release:</span> ' . htmlspecialchars($row->name) . '</div>';
						echo '</div>';
					echo '</a>';
				}
			}
		}else{
			switch($searchBy){
				case 'title':
					$sth = $dbh->prepare("SELECT releaseId,title FROM releasespatches WHERE type = ? AND title LIKE ? ORDER BY releaseId DESC, itemId ASC");
					break;
				case 'referenceId':
					$sth = $dbh->prepare("SELECT releaseId,title FROM releasespatches WHERE type = ? AND referenceId LIKE ? ORDER BY releaseId DESC, itemId ASC");
					break;
				case 'comments':
					$sth = $dbh->prepare("SELECT releaseId,title FROM releasespatches WHERE type = ? AND comments LIKE ? ORDER BY releaseId DESC, itemId ASC");
					break;
			}
			$sth->setFetchMode(PDO::FETCH_OBJ);
			$sth->execute(array(ucfirst($searchType), '%'.$keyword.'%'));
			$count = $sth->rowCount();
			if($count < 1){
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				echo '<div class="results-row-none">There are no results with "' . $keyword . '" in the ' . $searchBy . '!</div>';
			}else{
				echo '<input type="hidden" id="count" name="count" value="' . $count . '" />';
				while($row = $sth->fetch()){
					$releaseId = $row->releaseId;
					echo '<a href="releaseitems.php?releaseId=' . $releaseId . '">';
						echo '<div class="results-row">';
							echo '<div class="title"><span class="results-title">Title:</span> ' . htmlspecialchars($row->title) . '</div>';
							$sthRelease = $dbh->prepare("SELECT name FROM releases WHERE id = ? ORDER BY id DESC");
							$sthRelease->setFetchMode(PDO::FETCH_OBJ);
							$sthRelease->execute(array($releaseId));
							while($rowRelease = $sthRelease->fetch()){
								echo '<div class="release"><span class="results-title">Release:</span> ' . htmlspecialchars($rowRelease->name) . '</div>';
							}
						echo '</div>';
					echo '</a>';
				}
			}
		}
	}
?>