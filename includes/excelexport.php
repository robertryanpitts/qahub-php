<?php
	ob_start();
	header('Content-Type: application/octet-stream; charset=utf-8');
	header('Content-Disposition: attachment; filename="releaselist_export_' . date('Y-m-d_H-i-s') . '.csv"');
	header('Pragma: no-cache');
	header('Expires: 0');
	
	require_once('../includes/config.php');
	require_once('../authorize.php');

	$sthItem = $dbh->prepare("SELECT i.*,b.affectedBrowsers,d.affectedDesigns FROM releaseitems AS i LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedBrowsers FROM affectedbrowsers GROUP BY itemId) AS b ON i.itemId = b.itemId LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedDesigns FROM affecteddesigns GROUP BY itemId) AS d ON i.itemId = d.itemId WHERE i.releaseId = ? ORDER BY i.itemId ASC");
	$sthItem->setFetchMode(PDO::FETCH_OBJ);
	$sthItem->execute(array($_GET['releaseId']));

	$sthPatch = $dbh->prepare("SELECT p.*,b.affectedBrowsers,d.affectedDesigns FROM releasespatches AS p LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedBrowsers FROM affectedbrowsers GROUP BY itemId) AS b ON p.itemId = b.itemId LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedDesigns FROM affecteddesigns GROUP BY itemId) AS d ON p.itemId = d.itemId WHERE p.releaseId = ? ORDER BY p.itemId ASC");
	$sthPatch->setFetchMode(PDO::FETCH_OBJ);
	$sthPatch->execute(array($_GET['releaseId']));

	$csv = '"Type","Reference Type","Reference ID","Title","Affected Product","Affected Browser(s)","Affected Design(s)","Risk Level","Comfort Level","Comments","Edited Date","Edited By"' . "\r\n";
	while($rowItem = $sthItem->fetch()){
		$csv .= '"Item",';
		$csv .= '"' . $rowItem->referenceType . '",';
		$csv .= '"' . $rowItem->referenceId . '",';
		$csv .= '"' . $rowItem->title . '",';
		$csv .= '"' . $rowItem->affectedProduct . '",';
		$csv .= '"' . $rowItem->affectedBrowsers . '",';
		$csv .= '"' . $rowItem->affectedDesigns . '",';
		$csv .= '"' . $rowItem->riskLevel . '",';
		$csv .= '"' . $rowItem->comfortLevel . '",';
		$csv .= '"' . $rowItem->comments . '",';
		$csv .= '"' . $rowItem->editedDate . '",';
		$csv .= '"' . $rowItem->editedBy . '"' . "\r\n";
	}
	while($rowPatch = $sthPatch->fetch()){
		$csv .= '"' . $rowPatch->type . '",';
		$csv .= '"' . $rowPatch->referenceType . '",';
		$csv .= '"' . $rowPatch->referenceId . '",';
		$csv .= '"' . $rowPatch->title . '",';
		$csv .= '"' . $rowPatch->affectedProduct . '",';
		$csv .= '"' . $rowPatch->affectedBrowsers . '",';
		$csv .= '"' . $rowPatch->affectedDesigns . '",';
		$csv .= '"' . $rowPatch->riskLevel . '",';
		$csv .= '"' . $rowPatch->comfortLevel . '",';
		$csv .= '"' . $rowPatch->comments . '",';
		$csv .= '"' . $rowPatch->editedDate . '",';
		$csv .= '"' . $rowPatch->editedBy . '"' . "\r\n";
	}

	echo $csv;
	require_once('../includes/closeconn.php');
	ob_end_flush();
?>