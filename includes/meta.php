<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="noidex,nofollow" />
<?php
	if(!$_COOKIE['user']){
		setcookie('selectedColorScheme','default',time()+60*60*4);
		echo '<link rel="stylesheet" media="screen" href="css/style_default.css" type="text/css" />';
	}else if(!$_COOKIE['selectedColorScheme']){
		$selectedColorSchemeUser = $_COOKIE['user'];
		$sth = $dbh->prepare("SELECT selectedColorScheme FROM users WHERE username = ? LIMIT 1");
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$sth->execute(array($selectedColorSchemeUser));
		while($row = $sth->fetch()){
			echo '<link rel="stylesheet" media="screen" href="css/style_'. $row->selectedColorScheme .'.css" type="text/css" />';
		}
		setcookie('selectedColorScheme',$row->selectedColorScheme,time()+60*60*4);
	}else{
		echo '<link rel="stylesheet" media="screen" href="css/style_'. $_COOKIE['selectedColorScheme'] .'.css" type="text/css" />';
	}
?>
<link rel="stylesheet" media="screen" href="css/fancybox.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/navmenu.js"></script>
<script type="text/javascript" src="js/submitdata.js"></script>
<script type="text/javascript" src="js/jquery.watermark.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.min.js"></script>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/wysiwyg_init.js"></script>