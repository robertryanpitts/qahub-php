<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	($_GET['applicable'] == 'false') ? $sql = "UPDATE " . $_GET['table'] . " SET applicable = ?, value = 'false', editedBy = '" . $_COOKIE['user'] . "', editedDate = '" . date("Y-m-d  H:i:s") . "' WHERE releaseId = ? AND rowId = ?": $sql = "UPDATE " . $_GET['table'] . " SET applicable = ?, editedBy = '" . $_COOKIE['user'] . "', editedDate = '" . date("Y-m-d  H:i:s") . "' WHERE releaseId = ? AND rowId = ?";
	$sth = $dbh->prepare($sql);
	$sth->execute(array($_GET['applicable'], $_GET['releaseId'], $_GET['rowId']));
	require_once('includes/closeconn.php');
	$arr = array('releaseId' => $_GET['releaseId'], 'table' => $_GET['table'], 'rowId' => $_GET['rowId'], 'applicable' => $_GET['applicable']);
	echo json_encode($arr);
?>