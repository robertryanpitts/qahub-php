<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Set New Password</title>
	<?php
		require_once('includes/meta_login.php');
	?>
	<script type="text/javascript" src="js/password.validator.js"></script>
</head>
<body>
	<div id="set-new-password">
		<div class="title-message left">
			<span class="login-title">
				QA Hub
			</span>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="reset-password-form">
			<?php
				$editedDate = date('Y-m-d  H:i:s');
				$username = $_GET['user'];
				$remember = $_GET['remember'];
			?>
			<span class="reset-title left">
				Set a New Password
			</span>
			<div class="clear"></div>
			<div class="message left"></div>
			<div class="clear"></div>
			<form name="reset_pass" method="post" action="resetpasspost.php">
				<div class="form-row">
					Password:&nbsp;&nbsp;&nbsp;<input type="password" class="new-password password" name="password" />
				</div>
				<div class="form-row form-btns">
					<input type="text" id="bot-catcher" name="BotCatcher" />
					<input name="editedDate" type="hidden" value="<?php echo $editedDate; ?>" />
					<input name="username" type="hidden" value="<?php echo $username; ?>" />
					<input name="firstTimeUser" type="hidden" value="true" />
					<input name="remember" type="hidden" value="<?php echo $remember; ?>" />
					<input class="input-btn reset-password-submit" type="submit" name="reset_pass" value="Update Password" />
					<span class="disabled-message"></div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>