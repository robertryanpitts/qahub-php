<?php
	require_once('includes/config.php');
	require_once('authorize.php');

	if(isset($_POST['edit'])){
		$sth = $dbh->prepare("UPDATE releases SET id = ?, name = ?, owner = ?, backupContact = ?, dateCreated = ?, modifiedBy = ?, editedDate = ?, dateDeploy = ?, dateCodeCutoff = ?, active = ? WHERE id = ?");
		$sth->execute(array($_POST['id'], $_POST['name'], $_POST['owner'], $_POST['backupContact'], $_POST['dateCreated'], $_POST['modifiedBy'], $_POST['editedDate'], str_replace('/', '-', $_POST['dateDeploy']), str_replace('/', '-', $_POST['dateCodeCutoff']), $_POST['active'], $_POST['id']));
	}
	require_once('includes/closeconn.php');
	header('location:releases.php?edit=success');
?>