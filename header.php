<div id="header-wrapper" class="left">
	<a href="home.php">
		<h1 class="header-logo left">
			QA Hub
			<span class="version">
				v2.1
			</span>
		</h1>
	</a>
	<?php
		$sth = $dbh->prepare("SELECT id,username from users WHERE username = ? LIMIT 1");
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$sth->execute(array($_COOKIE['user']));
		while($row = $sth->fetch()){
			echo '<h3 class="right">Welcome, <a href="useredit.php?id=' . $row->id . '">' . $row->username . '</a>! <a href="logout.php">Logout</a>?</h3>';
		}
	?>
</div>
<div class="clear"></div>