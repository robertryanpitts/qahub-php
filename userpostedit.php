<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	
	if(isset($_POST['edit'])){
		($_COOKIE['user'] == $_POST['username']) ? setcookie('selectedColorScheme',$_POST['colorScheme']) : '';
		$data = array($_POST['username'], $_POST['firstName'], $_POST['lastName'], $_POST['nickname'], $_POST['email'], $_POST['colorScheme'], $_POST['admin'], $_POST['active'], $_POST['editedDate'], $_POST['id']);
		$sth = $dbh->prepare("UPDATE users SET username = ?, firstName = ?, lastName = ?, nickname = ?, email = ?, selectedColorScheme = ?, isAdmin = ?, active = ?, editedDate = ? WHERE id = ?");
		$sth->execute($data);
		if($_POST['active'] = 'false'){
			$to = $_POST['email'];
			$subject = 'Your QA Hub Account Has Been Deactivated';
			$headers = 'MIME-Version: 1.0' . '\r\n';
			$headers .= 'Content-type:text/html;charset=iso-8859-1' . '\r\n';
			$headers .= 'from: QA Hub Admin <RPitts@ClickMotive.com>';
			$message = '<span style="font-size:26px; font-weight:bold; color:#0099FF;">QA Hub - Account Deactivated</span><br /><br />' . '\r\n';
			$message .= '<span style="font-size:18px; color:#555555;">$nickname, your QA Hub account has been deactivated.  Don\'t worry, nothing has been deleted from your account.  You just will not be able to log in.  If you think that this has been a mistake please contact your QA Hub administrator at <a href="mailto:RPitts@ClickMotive.com">RPitts@ClickMotive.com</a></span><br /><br />' . '\r\n';
			$message .= '<span style="font-size:22px; color:#555555;">Thanks,<br /><strong><a href="http://qahub.clicmotivedev.com" style="color:#0099FF; text-decoration:none;">QA Hub</a></strong></span>';
			mail($to,$subject,$message,$headers);
		}
		require_once('includes/closeconn.php');
		header('location:users.php?edit=success&user=' . $_POST['nickname']);
	}
?>