<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	
	if(isset($_POST['upload'])){
		$data = array($_POST['username'], $_POST['firstName'], $_POST['lastName'], $_POST['nickname'], md5('password'), $_POST['email'], $_POST['colorScheme'], $_POST['admin'],$_POST['active'], $_POST['dateCreated'],$_POST['editedDate']);
		$sth = $dbh->prepare("INSERT INTO users (username, firstName, lastName, nickname, password, email, selectedColorScheme, isAdmin, active, dateCreated, editedDate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$sth->execute($data);
		if($_POST['active'] = 'true'){
			$to = $_POST['email'];
			$subject = 'Your QA Hub Admin Login Info';
			$headers = 'MIME-Version: 1.0' . '\r\n';
			$headers .= 'Content-type:text/html;charset=iso-8859-1' . '\r\n';
			$headers .= 'from: QA Hub Admin <RPitts@ClickMotive.com>';
			$message = '<span style="font-size:26px; font-weight:bold; color:#0099FF;">QA Hub - Admin Login Info</span><br /><br />' . '\r\n';
			$message .= '<span style="font-size:18px; font-weight:bold; color:#555555;">Username: $username<br />Password: $password</span><br /><br />' . '\r\n';
			$message .= '<span style="font-size:18px; font-weight:bold; color:#555555;">Login Setup Instructions:<br /><ol><li>Click the link below to go to the login screen.</li><li>Enter your username and temporary password.</li><li>When prompted, set a new password.</li><li>That\'s it! You\'re all set up!</li></ol>' . '\r\n';
			$message .= '<span style="font-size:18px; font-weight:bold; color:#555555;"><a href="http://qahub.clicmotivedev.com" target="_blank">Click here</a> to finish setting up your account.</span><br /><br />' . '\r\n';
			$message .= '<span style="font-size:22px; color:#555555;">Thanks,<br /><strong><a href="http://qahub.clicmotivedev.com" style="color:#0099FF; text-decoration:none;">QA Hub</a></strong></span>';
			mail($to,$subject,$message,$headers);
		}
		require_once('includes/closeconn.php');
		header('location:users.php?add=success&user=' . $_POST['nickname'] . '&email=' . $_POST['email']);
	}
?>