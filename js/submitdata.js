var updateUserDateTools = function updateUserDateTools(data){
	var releaseId = data.releaseId;
	var table = data.table;
	var label = data.label;
	var value = data.value;
	var editedDate = data.editedDate;
	var editedBy = data.editedBy;
	$('#tools .checklist-edited-data .checklist-new-edited-date').fadeOut(250, function() {
		$(this).html(editedDate).fadeIn(250);
	});
	$('#tools .checklist-edited-data .checklist-new-edited-by').fadeOut(250, function() {
		$(this).html(editedBy).fadeIn(250);
	});
	if($('#tools .checklist-edited-data').text() == ''){
		$('#tools .checklist-edited-data').hide().html(' - <span class="checklist-edit-date">(last updated on <span class="checklist-new-edited-date">' + editedDate + '</span> by <span class="checklist-new-edited-by">' + editedBy + '</span>)</span></span>').fadeIn(250);
	}
	$('#tools img.tool').each(function(){
		if($(this).attr('label') == label && $(this).attr('value') == 'false'){
			$(this).fadeOut(250, function(){
				$(this).attr('src','images/' + label + '_true.png').fadeIn(250);
			});
			$(this).attr('value',value);
		}else if($(this).attr('label') == label && $(this).attr('value') == 'true'){
			$(this).fadeOut(250, function(){
				$(this).attr('src','images/' + label + '_false.png').fadeIn(250);
			});
			$(this).attr('value',value);
		};
	});
}

var submitDataTools = function submitDataTools(target){
	var release = target.attr('release');
	var tableName = target.attr('table');
	var label = target.attr('label');
	var value = target.attr('value');
	if(value == 'true'){
		value = 'false';
	}else if(value == 'false'){
		value = 'true';
	}
	$.ajax({
		type: 'GET',
		url: 'checklisttoolspost.php?releaseId=' + release + '&table=' + tableName + '&label=' + label + '&value=' + value,
		success: function(data){
			updateUserDateTools(data);
		},
		error: function(){
			alert('There was an error submitting data to the database.  Please contact Ryan to have this issue resolved.');
		},
		dataType: 'json'
	}); 
}

var updateUserDate = function updateUserDate(data){
	var table = data.table;
	var rowId = data.rowId;
	var editedDate = data.editedDate;
	var editedBy = data.editedBy;
	$('#' + table + ' .row' + rowId + ' .checklist-new-edited-date').fadeOut(250, function() {
		$(this).html(editedDate).fadeIn(250);
	});
	$('#' + table + ' .row' + rowId + ' .checklist-new-edited-by').fadeOut(250, function() {
		$(this).html(editedBy).fadeIn(250);
	});
	if($('#' + table + ' .row' + rowId + ' .checklist-row-edited-data').text() == ''){
		$('#' + table + ' .row' + rowId + ' .checklist-row-edited-data').hide().html(' - <span class="checklist-edit-date">(last updated on <span class="checklist-new-edited-date">' + editedDate + '</span> by <span class="checklist-new-edited-by">' + editedBy + '</span>)</span></span>').fadeIn(250);
	}
}

var submitDataCheckbox = function submitDataCheckbox(target){
	var release = target.attr('release');
	var tableName = target.attr('table');
	var rowId = target.attr('rowId');
	var checkboxValue = (target.attr('checked'))?true:false;
	$.ajax({
		type: 'GET',
		url: 'checklistpost.php?releaseId=' + release + '&table=' + tableName + '&rowId=' + rowId + '&value=' + checkboxValue,
		success: function(data){
			markCompleted();
			updateUserDate(data);
		},
		error: function(){
			alert('There was an error submitting data to the database.  Please contact Ryan to have this issue resolved.');
		},
		dataType: 'json'
	}); 
}

var updateUserDateDelete = function updateUserDateDelete(data){
	var releaseId = data.releaseId;
	var tableName = data.tableName;
	var rowId = data.rowId;
	var commentId = data.commentId;
	var editedDate = data.editedDate;
	var editedBy = data.editedBy;
	$('#' + tableName + ' .row' + rowId + ' .checklist-new-edited-date').fadeOut(250, function() {
		$(this).html(editedDate).fadeIn(250);
	});
	$('#' + tableName + ' .row' + rowId + ' .checklist-new-edited-by').fadeOut(250, function() {
		$(this).html(editedBy).fadeIn(250);
	});
	if($('#' + tableName + ' .row' + rowId + ' .checklist-edited-data').text() == ''){
		$('#' + tableName + ' .row' + rowId + ' .checklist-edited-data').hide().html(' - <span class="checklist-edit-date">(last updated on <span class="checklist-new-edited-date">' + editedDate + '</span> by <span class="checklist-new-edited-by">' + editedBy + '</span>)</span></span>').fadeIn(250);
	}
	$('#' + tableName + ' li[class="comment-box-edit' + rowId + '"][comment="' + commentId + '"]').fadeOut(250, function(){
		$('#' + tableName + ' li[class="comment-box-edit' + rowId + '"][comment="' + commentId + '"]').remove();
	});
}

var commentDelete = function commentDelete(releaseId,tableName,rowId,commentId){
	$.ajax({
		type: 'GET',
		url: 'checklistcommentdelete.php?releaseId=' + releaseId + '&tableName=' + tableName + '&rowId=' + rowId + '&commentId=' + commentId,
		success: function(data){
			updateUserDateDelete(data);
		},
		error: function(){
			alert('There was an error submitting data to the database.  Please contact Ryan to have this issue resolved.');
		},
		dataType: 'json'
	});
}

var applicableTrue = function applicableTrue(release,table,rowId){
	var applicable = 'true';
	$.ajax({
		type: 'GET',
		url: 'applicablepost.php?releaseId=' + release + '&table=' + table + '&rowId=' + rowId + '&applicable=' + applicable,
		success: function(data){
			window.location.reload();
		},
		error: function(){
			alert('There was an error submitting data to the database.  Please contact Ryan to have this issue resolved.');
		},
		dataType: 'json'
	});
}

var applicableFalse = function applicableFalse(release,table,rowId){
	var applicable = 'false';
	$.ajax({
		type: 'GET',
		url: 'applicablepost.php?releaseId=' + release + '&table=' + table + '&rowId=' + rowId + '&applicable=' + applicable,
		success: function(data){
			window.location.reload();
		},
		error: function(){
			alert('There was an error submitting data to the database.  Please contact Ryan to have this issue resolved.');
		},
		dataType: 'json'
	});
}

var resetForm = function resetForm(releaseId,table){
	$.ajax({
		type: 'GET',
		url: 'resetform.php?releaseId=' + releaseId + '&table=' + table,
		success: function(){
			//refresh page
			window.location.reload();
		},
		error: function(){
			alert('There was an error submitting data to the database.  Please contact Ryan to have this issue resolved.');
		},
		dataType: 'text/html'
	});
}

var resetRelease = function resetRelease(releaseId){
	$.ajax({
		type: 'GET',
		url: 'resetrelease.php?releaseId=' + releaseId,
		success: function(){
			//refresh page
			window.location.reload();
		},
		error: function(){
			alert('There was an error submitting data to the database.  Please contact Ryan to have this issue resolved.');
		},
		dataType: 'text/html'
	});
}