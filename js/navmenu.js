// toggles the state of the menu item clicked on
var toggleMenuItem = function toggleMenuItem(elem){
	if(!$(elem).next('.sub-menu').hasClass('active')){
		collapseAllMenuItems();
		$(elem).next('.sub-menu').animate({
			height: 'toggle'
			}, 100, function(){
					$(this).addClass('active');
				}
		);
	}
}

// closes all menu items that may be open
var collapseAllMenuItems = function collapseAllMenuItems(){
	$('#nav-wrapper > .menu > li').each(function(){
		if($(this).next('.sub-menu').hasClass('active')){
			$(this).next('.sub-menu').animate({
				height: 'toggle'
				}, 100, function(){
					$(this).removeClass('active');
				}
			);
		}
	});
}

$(document).ready(function(){
	// adds the listeners to the menu items
	$('#nav-wrapper > .menu > li').live('click', function(){
		toggleMenuItem(this);
	});
});