$(document).ready(function(){
	var strPassword;
	var charPassword;
	var submitBtn = $('.reset-password-submit');
	var complexity = $('.message');
	var minPasswordLength = 8;
	var baseScore = 0, score = 0;
	
	var num = {};
	num.Excess = 0;
	num.Upper = 0;
	num.Numbers = 0;
	num.Symbols = 0;

	var bonus = {};
	bonus.Excess = 3;
	bonus.Upper = 4;
	bonus.Numbers = 5;
	bonus.Symbols = 5;
	bonus.Combo = 0; 
	bonus.FlatLower = 0;
	bonus.FlatNumber = 0;
	
	outputResult();
	$('.password').bind('keyup', checkVal);

	function checkVal(){
		init();
		
		if(charPassword.length >= minPasswordLength){
			baseScore = 50;	
			analyzeString();	
			calcComplexity();		
		}else{
			baseScore = 0;
		}

		if(submitBtn.attr('disabled')){
			$('.disabled-message').html('Submit button disabled!');
		}else{
			$('.disabled-message').html('');
		}
		
		outputResult();
	}

	function init(){
		strPassword= $('.password').val();
		charPassword = strPassword.split('');
			
		num.Excess = 0;
		num.Upper = 0;
		num.Numbers = 0;
		num.Symbols = 0;
		bonus.Combo = 0; 
		bonus.FlatLower = 0;
		bonus.FlatNumber = 0;
		baseScore = 0;
		score =0;
	}

	function analyzeString (){	
		for(i=0; i<charPassword.length;i++){
			if (charPassword[i].match(/[A-Z]/g)) {num.Upper++;}
			if (charPassword[i].match(/[0-9]/g)) {num.Numbers++;}
			if (charPassword[i].match(/(.*[!,@,#,$,%,^,&,*,?,_,~])/)) {num.Symbols++;} 
		}
		
		num.Excess = charPassword.length - minPasswordLength;
		
		if(num.Upper && num.Numbers && num.Symbols){
			bonus.Combo = 25; 
		}else if((num.Upper && num.Numbers) || (num.Upper && num.Symbols) || (num.Numbers && num.Symbols)){
			bonus.Combo = 15; 
		}
		
		if(strPassword.match(/^[\sa-z]+$/)){ 
			bonus.FlatLower = -15;
		}
		
		if(strPassword.match(/^[\s0-9]+$/)){ 
			bonus.FlatNumber = -35;
		}
	}
		
	function calcComplexity(){
		score = baseScore + (num.Excess*bonus.Excess) + (num.Upper*bonus.Upper) + (num.Numbers*bonus.Numbers) + (num.Symbols*bonus.Symbols) + bonus.Combo + bonus.FlatLower + bonus.FlatNumber;
		
	}	
		
	function outputResult(){
		if($('.password').val()== ''){ 
			complexity.html('Password strength: weak!').removeClass('password-weak password-strong password-stronger password-strongest').addClass('password-weak');
			submitBtn.attr('disabled', 'disabled');
		}else if(charPassword.length < minPasswordLength){
			complexity.html('Password must have at least ' + minPasswordLength+ ' characters!').removeClass('password-strong password-stronger password-strongest').addClass('password-weak');
			submitBtn.attr('disabled', 'disabled');
		}else if(score < 50){
			complexity.html('Your password is weak!').removeClass('password-strong password-stronger password-strongest').addClass('password-weak');
			submitBtn.removeAttr('disabled');
		}else if(score >= 50 && score < 75){
			complexity.html('Your password is average!').removeClass('password-stronger password-strongest').addClass('password-strong');
			submitBtn.removeAttr('disabled');
		}else if(score >= 75 && score < 100){
			complexity.html('Your password is strong!').removeClass('password-strongest').addClass('password-stronger');
			submitBtn.removeAttr('disabled');
		}else if(score >= 100){
			complexity.html('Your password is secure!').addClass('password-strongest');
			submitBtn.removeAttr('disabled');
		}
	}
}); 