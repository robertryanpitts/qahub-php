// Load Tiny_MCE WYSIWYG Editor
tinyMCE.init({
	mode : 'exact',
	elements : "comment-textarea",
	auto_focus : "comment-textarea",
	theme : 'advanced',
	plugins : 'spellchecker,insertdatetime,preview,advhr',
	height : "360",
	width : "625",

	theme_advanced_buttons1 : 'bold,italic,underline,|,removeformat,code,preview,|,cut,copy,paste,|,undo,redo,|,link,unlink,anchor',
	theme_advanced_buttons2 : 'forecolor,backcolor,|,bullist,numlist,|,outdent,indent,|,spellchecker,advhr,|,insertdate,inserttime,|,sub,sup,|,charmap',
	theme_advanced_buttons3 : '',

	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,

	relative_urls : false,
	remove_script_host : false,
	convert_urls : false
});

tinyMCE.init({
	mode : 'exact',
	elements : "note-textarea",
	auto_focus : "note-textarea",
	theme : 'advanced',
	plugins : 'spellchecker,insertdatetime,preview,advhr',
	height : "450",
	width : "750",

	theme_advanced_buttons1 : 'bold,italic,underline,|,removeformat,code,preview,|,cut,copy,paste,|,undo,redo,|,link,unlink,anchor',
	theme_advanced_buttons2 : 'forecolor,backcolor,|,bullist,numlist,|,outdent,indent,|,spellchecker,advhr,|,insertdate,inserttime,|,sub,sup,|,charmap',
	theme_advanced_buttons3 : '',

	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,

	relative_urls : false,
	remove_script_host : false,
	convert_urls : false
});