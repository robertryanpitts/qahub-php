<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	$editedDate = date('Y-m-d  H:i:s');
	$sql = "UPDATE " . $_GET['table'] . " SET value = ?, editedBy = ?, editedDate = ? WHERE releaseId = ? AND label = ?";
	$sth = $dbh->prepare($sql);
	$sth->execute(array($_GET['value'], $_COOKIE['user'], $editedDate, $_GET['releaseId'], $_GET['label']));
	$sth = $dbh->prepare("UPDATE releases SET modifiedBy = ?, editedDate = ? WHERE id = ?");
	$sth->execute(array($_COOKIE['user'], $editedDate, $_GET['releaseId']));
	$sth = $dbh->prepare("UPDATE releases SET modifiedBy = ?, editedDate = ? WHERE id = ?");
	$sth->execute(array($_COOKIE['user'], $editedDate, $_GET['releaseId']));
	require_once('includes/closeconn.php');
	$arr = array ('releaseId' => $_GET['releaseId'], 'table' => $_GET['table'], 'label' => $_GET['label'], 'value' => $_GET['value'], 'editedDate' => date('F j, Y @ g:ia', strtotime($editedDate)), 'editedBy' => $_COOKIE['user']);
	echo json_encode($arr);
?>