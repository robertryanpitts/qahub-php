<?php
	require_once('includes/config.php');
	require_once('includes/config.php');

	if(isset($_POST['edit'])){
		$sth = $dbh->prepare("UPDATE releasespatches SET releaseId = ?, type = ?, referenceType = ?, referenceId = ?, title = ?, dataChange = ?, configChange = ?, affectedProduct = ?, comfortLevel = ?, riskLevel = ?, comments = ?, editedBy = ?, editedDate = ? WHERE itemId = ?");
		$sth->execute(array($_POST['releaseId'], $_POST['type'], $_POST['referenceType'], $_POST['referenceId'], $_POST['title'], $_POST['dataChange'], $_POST['configChange'], $_POST['affectedProduct'], $_POST['comfortLevel'], $_POST['riskLevel'], $_POST['comments'], $_POST['editedBy'], $_POST['editedDate'], $_POST['itemId']));
		$sth = $dbh->prepare("DELETE FROM affectedbrowsers WHERE itemId = ? AND type = ?");
		$sth->execute(array($_POST['itemId'], $_POST['type']));
		foreach($_POST['affectedbrowsers'] as $affectedBrowsers){
			$sth = $dbh->prepare("INSERT INTO affectedbrowsers (releaseId, itemId, type, label, editedBy, editedDate) VALUES (?, ?, ?, ?, ?, ?)");
			$sth->execute(array($_POST['releaseId'], $_POST['itemId'], $_POST['type'], $affectedBrowsers, $_POST['editedBy'], $_POST['editedDate']));
		}
		$sth = $dbh->prepare("DELETE FROM affecteddesigns WHERE itemId = ? AND type = ?");
		$sth->execute(array($_POST['itemId'], $_POST['type']));
		foreach($_POST['affecteddesigns'] as $affectedDesigns){
			$sth = $dbh->prepare("INSERT INTO affecteddesigns (releaseId, itemId, type, label, editedBy, editedDate) VALUES (?, ?, ?, ?, ?, ?)");
			$sth->execute(array($_POST['releaseId'], $_POST['itemId'], $_POST['type'], $affectedDesigns, $_POST['editedBy'], $_POST['editedDate']));
		}
		$sth = $dbh->prepare("UPDATE releases SET modifiedBy = ?, editedDate = ? WHERE id = ?");
		$sth->execute(array($_POST['editedby'], $_POST['editedDate'], $_POST['releaseId']));
	}
	require_once('includes/closeconn.php');
	header('location:releaseitems.php?releaseId=' . $_POST['releaseId'] . '&type=' . $_POST['type'] . '&editItem=success');
?>