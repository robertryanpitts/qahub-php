<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	if($_GET['commentType'] == 'Add'){
		$sth = $dbh->prepare("SELECT * FROM comments ORDER BY commentId DESC LIMIT 1");
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$sth->execute();
		while($row = $sth->fetch()){
			$editedDateCheck = strtotime($row->editedDate);
			$currentTime = time() - 500;
			($editedDateCheck > $currentTime) ? $editedDate = date('F j, Y @ g:ia', strtotime($row->editedDate)) : $editedDate = 0;
			$arr = array ('tableName' => $row->tableName, 'releaseId' => $row->releaseId, 'rowId' => $row->rowId, 'commentId' => $row->commentId, 'comment' => $row->comment, 'editedDate' => $row->editedDate, 'editedBy' => $row->editedBy);
		echo json_encode($arr);
		}
	}else if($_GET['commentType'] == 'Edit'){
		$sth = $dbh->prepare("SELECT * FROM comments WHERE commentId = ?");
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$sth->execute(array($_GET['commentId']));
		while($row = $sth->fetch()){
			$arr = array ('tableName' => $row->tableName, 'releaseId' => $row->releaseId, 'rowId' => $row->rowId, 'commentId' => $row->commentId, 'comment' => $row->comment, 'editedDate' => date('F j, Y @ g:ia', strtotime($row->editedDate)), 'editedBy' => $row->editedBy);
			echo json_encode($arr);
		}
	}
	require_once('includes/closeconn.php');
?>