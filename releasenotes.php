<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	$releaseId = $_GET['releaseId'];
	$releases = mysql_query("SELECT name FROM releases WHERE id = '$releaseId' LIMIT 1") or die('Error: ' . mysql_error());
	$release = mysql_fetch_assoc($releases);
	$result = mysql_query("SELECT * FROM releasenotes WHERE releaseId = '$releaseId' ORDER BY editedDate DESC LIMIT 1") or die('Error: ' . mysql_error());
	$row = mysql_fetch_assoc($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Notes for: <?php echo $release['name'] ?> </title>
	<?php
		require_once('includes/meta.php');
		echo '<script type="text/javascript">';
			echo 'var releaseId = ' . $releaseId . ';';
			if($row['noteId']){
				echo 'var noteId = ' . $row['noteId'] . ';';
			}else{
				echo 'var noteId = 0;';
			}
		echo '</script>';
	?>
	<script type="text/javascript">
		$('.note-update').fancybox({
			autoSize: false,
			height: 540,
			width: 775,
			type: 'iframe',
			padding: 15,
			afterClose: function(){
				$.ajax({
					type: 'GET',
					url: 'releasenotesgetdata.php?releaseId=' + releaseId,
					success: function(data){
						if(data.note){
							$('#release-note').fadeOut(250, function(){
								$(this).html(data.note).fadeIn(250);
							});
							if($('#note-edited-data').html().length < 10){
								$('#note-edited-data').fadeOut(250, function(){
									$(this).html('This note was last updated on <span class="note-edited-date">' + data.editedDate + '</span> by <span class="note-edited-by">' + data.editedBy + '</span>').fadeIn(250);
								});
							}else{
								$('#note-edited-data .note-edited-date').fadeOut(250, function(){
									$(this).html(data.editedDate).fadeIn(250);
								});
								$('#note-edited-data .note-edited-by').fadeOut(250, function(){
									$(this).html(data.editedBy).fadeIn(250);
								});
							}
							getUpdateButtonParams(data.releaseId,data.noteId);
						}
					},
					error: function(){
						alert('There was an error getting data from the database.  Please contact Ryan to have this issue resolved.');
					},
					dataType: 'json'
				});
				getNoteRevisions(releaseId,1);
			}
		});

		var getUpdateButtonParams = function getUpdateButtonParams(release,note){
			$('a[name="note-update"]').attr('href','releasenotesupdate.php?releaseId=' + release + '&noteId=' + note);
		}

		var getNoteRevisions = function getNoteRevisions(release,page){
			$.ajax({
				type: 'GET',
				url: 'releasenotesgetrevisionsdata.php?releaseId=' + release + '&page=' + page,
				success: function(data){
					$('#note-history .note-revisions-list').html('');
					if(data.notes){
						$.each(data.notes, function(i,note){
							$('#note-history .note-revisions-list').append('<div class="revision"><a class="note-update" name="note-revision" href="releasenotesupdate.php?releaseId=' + note.releaseId + '&noteId=' + note.noteId + '">Updated on ' + note.editedDate + ' by ' + note.editedBy + '</a></div>');
						});
						var revisionTotal = data.total;
						var revisionStart = data.revisionStart+1;
						var revisionCount = data.perPage*page;
						var revisionEnd = revisionCount;
						if(revisionEnd > revisionTotal){
							revisionEnd = revisionTotal;
						}
						$('#note-history .revision-pages').html('Showing Revisions ' + revisionStart + '-' + revisionEnd + ' out of ' + revisionTotal);
						if(revisionStart == 1 && revisionEnd == revisionTotal){
							var nextPage = page+1;
							$('#note-history .revision-pages-nav').html('');
						}else if(revisionStart == 1){
							var nextPage = page+1;
							$('#note-history .revision-pages-nav').html('[<a href="#" onclick="getNoteRevisions(' + releaseId + ',' + nextPage + '); return false;">next</a>]');
						}else if(revisionEnd == revisionTotal){
							var prevPage = page-1;
							$('#note-history .revision-pages-nav').html('[<a href="#" onclick="getNoteRevisions(' + releaseId + ',' + prevPage + '); return false;">prev</a>]');
						}else{
							var prevPage = page-1;
							var nextPage = page+1;
							$('#note-history .revision-pages-nav').html('[<a href="#" onclick="getNoteRevisions(' + releaseId + ',' + prevPage + '); return false;">prev</a> / <a href="#" onclick="getNoteRevisions(' + releaseId + ',' + nextPage + '); return false;">next</a>]');
						}
					}else{
						$('#note-history .note-revisions-list').html('There is no revision history for this note.');
						$('#note-history .revision-pages').html('');
					}
				},
				error: function(){
					alert('There was an error getting data from the database.  Please contact Ryan to have this issue resolved.');
				},
				dataType: 'json'
			});
		}

		$(document).ready(function(){
			getUpdateButtonParams(releaseId,noteId);
			getNoteRevisions(releaseId,1);
		});
	</script>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<h1 id="note-title" class="left">Notes for: <?php echo $release['name'] ?></h1>
				<h3 id="external-note-view" class="right">[<a href="external/releasenotes.php?releaseId=<?php echo $releaseId; ?>">External View</a>]</h3>
				<h3 id="note-edited-data" class="left">
					<?php
						if($row['note'] != ''){
							echo 'This note was last updated on <span class="note-edited-date">' . date('F j, Y @ g:ia', strtotime($row['editedDate'])) . '</span> by <span class="note-edited-by">' . $row['editedBy'] . '</span>';
						}
					?>
				</h3>
				<div id="container">
					<div id="release-note" class="left">
						<?php
							if($row['note'] != ''){
								echo $row['note'];
							}else{
								echo 'Notes have not been entered for this release yet.  Click the button below to add notes to this release.';
							}
						?>
						<div class="clear"></div>
					</div>
					<a class="note-update input-btn" name="note-update" href="#">Update Note</a>
					<div id="note-history">
						<span class="note-history-title">Revision History:</span>
						<div class="note-revisions-list"></div>
						<div class="revision-pages"></div>
						<div class="revision-pages-nav"></div>
					</div>
				</div>
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>