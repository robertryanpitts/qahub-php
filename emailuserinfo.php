<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Email Credentials</title>
	<?php
		require_once('includes/meta_login.php');
	?>
</head>
<body>
	<div id="login-wrapper">
		<div class="title-message left">
			<span class="login-title">
				QA Hub
			</span>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="get-password-form">
			<?php
				if($_GET['sent'] == "error"){
			?>
			<div class="message left">
				<span class="error-message">Your email doesn't appear to be in our database!</span>
			</div>
			<?php
				}
			?>
			<div class="clear"></div>
			<form name="get_info" method="post" action="sendemail.php">
				<div class="form-row">
					Email:&nbsp;&nbsp;&nbsp;<input type="text" id="email" class="get-password-email" name="email" />
				</div>
				<div class="form-row form-btns">
					<input type="text" id="bot-catcher" name="BotCatcher" />
					<input class="input-btn" type="submit" name="get_info" id="submit" value="Send Information" />
				</div>
			</form>
		</div>
	</div>
</body>
</html>