<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Edit Release</title>
	<?php
		require_once('includes/meta.php');
	?>
	<link type="text/css" href="js/datepicker/themes/smoothness/jquery.ui.all.css" rel="stylesheet" />
	<script type="text/javascript" src="js/datepicker/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="js/datepicker/ui/jquery-ui-1.8.4.custom.js"></script>
	<script type="text/javascript" src="js/datepicker/ui/jquery.ui.datepicker.js"></script>
	<script type="text/javascript">
		$(function() {
			$('#date-deploy').datepicker({
				showOn: 'button',
				buttonImage: 'images/calendar.gif',
				buttonImageOnly: true
			});
			$('#date-code-cutoff').datepicker({
				showOn: 'button',
				buttonImage: 'images/calendar.gif',
				buttonImageOnly: true
			});
		});
	</script>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
				$sth = $dbh->prepare("SELECT * FROM releases WHERE id = ? LIMIT 1");
				$sth->setFetchMode(PDO::FETCH_OBJ);
				$sth->execute(array($_GET['id']));
				while($row = $sth->fetch()){
					$id = $row->id;
					$name = htmlspecialchars($row->name);
					$owner = $row->owner;
					$backupContact = $row->backupContact;
					$dateDeploy = str_replace('-', '/', $row->dateDeploy);
					$dateCodeCutoff = str_replace('-', '/', $row->dateCodeCutoff);
					$dateCreated = $row->dateCreated;
					$modifiedBy = $_COOKIE['user'];
					$active = $row->active;
					$editedDate = date('Y-m-d  H:i:s');
				}
				$sth = $dbh->prepare("SELECT firstName,lastName FROM users ORDER BY firstName ASC");
				$sth->setFetchMode(PDO::FETCH_OBJ);
				$sth->execute();
				$owners = array();
				$backupContacts = array();
				while($row = $sth->fetch()){
					if($owner == $row->firstName . ' ' . $row->lastName){
						$owners[] =  '<option value="' . $row->firstName . ' ' . $row->lastName . '" selected="selected">' . $row->firstName . ' ' . $row->lastName . '</option>';
					}else{
						$owners[] =  '<option value="' . $row->firstName . ' ' . $row->lastName . '">' . $row->firstName . ' ' . $row->lastName . '</option>';
					}
					if($backupContact == $row->firstName . ' ' . $row->lastName){
						$backupContacts[] =  '<option value="' . $row->firstName . ' ' . $row->lastName . '" selected="selected">' . $row->firstName . ' ' . $row->lastName . '</option>';
					}else{
						$backupContacts[] =  '<option value="' . $row->firstName . ' ' . $row->lastName . '">' . $row->firstName . ' ' . $row->lastName . '</option>';
					}
				}
			?>
			<div id="main-content">
				<h1>
					<strong>Release Editor</strong>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="releases.php">Back to Releases Manager</a>
				</h1>
				<br />
				<form name="edit_release" method="post" action="releasepostedit.php">
					<div class="form-row">
						Name:
						<br />
						<input class="large-input" type="text" name="name" maxlength="60" value="<?php echo $name; ?>" />
					</div>
					<div class="form-row">
						Owner:
						<br />
						<select name="owner">
							<option value="Unassigned">Unassigned</option>
							<?php
								foreach($owners as $k => $v){
									echo $v;
								}
							?>
						</select>
					</div>
					<div class="form-row">
						Backup Contact:
						<br />
						<select name="backupContact">
							<option value="Unassigned">Unassigned</option>
							<?php
								foreach($backupContacts as $k => $v){
									echo $v;
								}
							?>
						</select>
					</div>
					<div class="form-row">
						Release Date:
						<br />
						<input name="dateDeploy" type="text" id="date-deploy" value="<?php echo $dateDeploy; ?>" />
					</div>
					<div class="form-row">
						Code Cutoff Date:
						<br />
						<input name="dateCodeCutoff" type="text" id="date-code-cutoff" value="<?php echo $dateCodeCutoff; ?>" />
					</div>
					<div class="form-row">
						Active:
						<br />
						<span class="tooltip">
							<img src="images/tooltip.png" /> inactive releases will not displayed on the checklists.php or releaselists.php pages
						</span>
						<br />
						<input type="radio" name="active" value="true" <?php echo ($active == 'true') ? 'checked="checked"' : ''; ?> />
						<strong>Active</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="active" value="false" <?php echo ($active == 'false') ? 'checked="checked"' : ''; ?> />
						<strong>Inactive</strong>
					</div>
					<div class="form-row">
						<input name="editedDate" type="hidden" value="<?php echo $editedDate ?>" />
						<input name="dateCreated" type="hidden" value="<?php echo $dateCreated ?>" />
						<input name="modifiedBy" type="hidden" value="<?php echo $modifiedBy ?>" />
						<input name="id" type="hidden" value="<?php echo $id ?>" />
						<input class="input-btn" type="submit" name="edit" value="Update" />
						<input class="input-btn btn-cancel" type="button" name="cancel" value="Cancel" onclick="history.go(-1)" />
					</div>
				</form>
				<br />
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>