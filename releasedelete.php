<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	$sth = $dbh->prepare("DELETE FROM releases WHERE id = ? LIMIT 1");
	$sth->execute(array($_GET['id']));
	$sth = $dbh->prepare("DELETE FROM checklistrelease WHERE releaseId = ?");
	$sth->execute(array($_GET['id']));
	$sth = $dbh->prepare("DELETE FROM checklistdeploy WHERE releaseId = ?");
	$sth->execute(array($_GET['id']));
	$sth = $dbh->prepare("DELETE FROM checklistproduction WHERE releaseId = ?");
	$sth->execute(array($_GET['id']));
	$sth = $dbh->prepare("DELETE FROM releaseitems WHERE releaseId = ?");
	$sth->execute(array($_GET['id']));
	$sth = $dbh->prepare("DELETE FROM releasespatches WHERE releaseId = ?");
	$sth->execute(array($_GET['id']));
	$sth = $dbh->prepare("DELETE FROM tools WHERE releaseId = ?");
	$sth->execute(array($_GET['id']));
	require_once('includes/closeconn.php');
	header('location:releases.php?delete=success');
?>