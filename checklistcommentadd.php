<?php
	require_once('authorize.php');
	require_once('includes/config.php');
	require_once('includes/meta.php');
?>
<form id="comment-form" name="CommentAdd" action="checklistcommentpost.php" method="post">
	<h2>Add a Comment</h2>
	<textarea id="comment-textarea" name="comment"></textarea>
	<input type="hidden" name="releaseId" value="<?php echo $_GET['releaseId']; ?>" />
	<input type="hidden" name="tableName" value="<?php echo $_GET['tableName']; ?>" />
	<input type="hidden" name="rowId" value="<?php echo $_GET['rowId']; ?>" />
	<br />
	<input class="input-btn" type="submit" name="CommentAdd" value="Add" />
</form>