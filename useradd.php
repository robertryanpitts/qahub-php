<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Add User</title>
	<?php
		require_once('includes/meta.php');
	?>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
				$dateCreated = date("Y-m-d  H:i:s");
				$editedDate = $dateCreated;
			?>
			<div id="main-content">
				<h1>
					<strong>Add User</strong>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="users.php">Back to Users Manager</a>
				</h1>
				<br />
				<form name="add_user" method="post" action="userpostadd.php">
					<div class="form-row">
						Username:
						<br />
						<input class="large-input" type="text" name="username" maxlength="25" />
					</div>
					<div class="form-row">
						First Name:
						<br />
						<input type="text" name="firstName" maxlength="45" />
					</div>
					<div class="form-row">
						Last Name:
						<br />
						<input type="text" name="lastName" maxlength="45" />
					</div>
					<div class="form-row">
						Nickname:
						<br />
						<input class="large-input" type="text" name="nickname" maxlength="50" />
					</div>
					<div class="form-row">
						Email:
						<br />
						<input class="large-input" type="text" name="email" maxlength="40" />
					</div>
					<div class="form-row">
						Set Color Scheme:
						<br />
						<select name="colorScheme">
							<?php
								$sth = $dbh->prepare("SELECT * FROM colorschemes WHERE active = 1 ORDER BY colorSchemeName ASC");
								$sth->setFetchMode(PDO::FETCH_OBJ);
								$sth->execute();
								while($row = $sth->fetch()){
									echo ($row->colorSchemeName == 'default') ? '<option value="' . $row->colorSchemeName . '" selected="selected">' . $row->friendlyName . '</option>' : '<option value="' . $row->colorSchemeName . '">' . $row->friendlyName . '</option>';
								}
							?>
						</select>
					</div>
					<?php
						if($_COOKIE['admin'] == 'true'){
					?>
					<div class="form-row">
						Make Administrator?
						<br />
						<input type='radio' name='admin' value='true' />
						<strong>Yes</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='radio' name='admin' value='false' />
						<strong>No</strong>
					</div>
					<div class="form-row">
						Active:
						<br />
						<span class="tooltip">
							<img src="images/tooltip.png" /> inactive users will not be able to log in
						</span>
						<br />
						<input type='radio' name='active' value='true' />
						<strong>Active</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='radio' name='active' value='false' />
						<strong>Inactive</strong>
					</div>
					<?php
						}else{
							echo '<input type="hidden" name="admin" value="false" />';
							echo '<input type="hidden" name="active" value="true" />';
						}
					?>
					<div class="form-row">
						<input name="dateCreated" type="hidden" value="<?php echo $dateCreated ?>" />
						<input name="editedDate" type="hidden" value="<?php echo $editedDate ?>" />
						<input class="input-btn" type="submit" name="upload" value="Add" />
						<input class="input-btn btn-cancel" type="button" name="cancel" value="Cancel" onclick="history.go(-1)" />
					</div>
				</form>
				<br />
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>