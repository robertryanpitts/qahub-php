<?php
	require_once('includes/config.php');
	
	$username = $_POST['User'];
	$password = $_POST['Pass'];
	$dateLastLoggedIn = date("Y-m-d  H:i:s");
	$sth = $dbh->prepare("SELECT username,active,isAdmin,selectedColorScheme FROM users WHERE username = ? AND password = ? LIMIT 1");
	$sth->setFetchMode(PDO::FETCH_OBJ);
	$sth->execute(array($username,md5($password)));
	if($sth->rowCount() > 0){
		while($row = $sth->fetch()){
			if($row->active == 'false'){
				header('location:index.php?active=false');
			}else{
				if($row->dateLastLoggedIn == '0000-00-00 00:00:00' || $password == 'password'){
					if($_POST['remember'] == true){
						header('location:resetpassnew.php?user=' . $username . '&remember=true');
					}else{
						header('location:resetpassnew.php?user=' . $username . '&remember=false');
					}
				}else{
					echo 'got here 1';
					if($row->isAdmin == 'true'){
					echo 'got here 2';
						setcookie('admin','true',time()+60*60*24*365);
					}
					if($_POST['remember'] == true){
					echo 'got here 3';
						setcookie('user',$username,time()+60*60*24*365);
					}else{
					echo 'got here 4';
						setcookie('user',$username,time()+60*60*4);
					}
					$sthUser = $dbh->prepare("UPDATE users SET dateLastLoggedIn = ?  WHERE username = ?");
					$sthUser->execute(array($dateLastLoggedIn,$username));
					header('location:home.php');
				}
			}
			
		}
	}else{
		header('location:index.php?auth=error');
	}
?>