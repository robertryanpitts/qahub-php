<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Search</title>
	<?php
		require_once('includes/meta.php');
	?>
	<script type="text/javascript">
		$(document).ready(function(){
			var searchType = $.cookie('searchType');
			var searchBy = $.cookie('searchBy');
			var searchKeyword = $.cookie('keyword');
			(!searchType || searchType == '' || searchType == 'null') ? $('.group-search-type input[value=\'release-item\']').attr('checked','checked') : $('.group-search-type input[value=\'' + searchType + '\']').attr('checked','checked');
			(!searchBy || searchBy == '' || searchBy == 'null') ? $('.group-search-by input[value=\'title\']').attr('checked','checked') : $('.group-search-by input[value=\'' + searchBy + '\']').attr('checked','checked');
			if(!searchKeyword || searchKeyword == '' || searchKeyword == 'null'){
				$('#search-input').val('');
				$('#search-input').watermark('Begin typing to search...');
			}else{
				$('#search-input').val(searchKeyword);
				getResults(searchKeyword);
			}
			$('.group-search-type input').click(function(){
				var searchKeyword = $('#search-input').val();
				getResults(searchKeyword);
			});
			$('.group-search-by input').click(function(){
				var searchKeyword = $('#search-input').val();
				getResults(searchKeyword);
			});
		});
		var getResults = function getResults(value){
			var searchTypeVal = '';
			$('.group-search-type input').each(function(){
				($(this).is(':checked')) ? searchTypeVal = $(this).val() : '';
			});
			var searchByVal = '';
			$('.group-search-by input').each(function(){
				($(this).is(':checked')) ? searchByVal = $(this).val() : '';
			});
			$.post('includes/ajax-search.php',{partialSearch:value,searchType:searchTypeVal,searchBy:searchByVal},function(data){
				$('#search-results').html(data);
				var resultsCount = $('#count').val();
				$('#results-count').html(resultsCount + ' Results Returned!');
				if(resultsCount < '1'){
					$.cookie('keyword', '');
				}else{
					var keyword = $('#search-input').val();
					$.cookie('keyword', keyword);
					$.cookie('searchType', searchTypeVal);
					$.cookie('searchBy', searchByVal);
				}
			});
		}
	</script>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<h1>Live Search</h1>
				<div id="container">
					<div class="group-search-type">
						<span class="search-title">Search Type:</span>
						<br />
						<input id="search-type" type="radio" name="searchType" value="data" /> Data&nbsp;&nbsp;
						<input id="search-type" type="radio" name="searchType" value="patch" /> Patch&nbsp;&nbsp;
						<input id="search-type" type="radio" name="searchType" value="release" /> Project&nbsp;&nbsp;
						<input id="search-type" type="radio" name="searchType" value="release-item" /> Release Item&nbsp;&nbsp;
						<input id="search-type" type="radio" name="searchType" value="upgrade" /> Upgrade
						<br /><br />
					</div>
					<div class="group-search-by">
						<span class="search-title">Search By:</span>
						<br />
						<input id="search-by" type="radio" name="searchBy" value="title" /> Title&nbsp;&nbsp;
						<input id="search-by" type="radio" name="searchBy" value="releaseName" /> Release Name&nbsp;&nbsp;
						<input id="search-by" type="radio" name="searchBy" value="referenceId" /> Reference ID&nbsp;&nbsp;
						<input id="search-by" type="radio" name="searchBy" value="affectedBrowsers" /> Affected Browsers&nbsp;&nbsp;
						<input id="search-by" type="radio" name="searchBy" value="affectedDesigns" /> Affected Designs&nbsp;&nbsp;
						<input id="search-by" type="radio" name="searchBy" value="comments" /> Comments
						<br /><br />
					</div>
					<span class="search-title">Keyword:</span>
					<br />
					<input id="search-input" class="large-input" name="query" type="text" onkeyup="getResults(this.value)" />
					<div id="results-count"></div>
					<div id="search-results"></div>
				</div>
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>