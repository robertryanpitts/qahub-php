<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Releases Manager</title>
	<?php
		require_once('includes/meta.php');
	?>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<h1>
					<strong>Manage Releases</strong>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="releaseadd.php">Add a Release</a>
				</h1>
				<br />
				<?php
					echo ($_GET['delete'] == 'success') ? '<div class="success-message">Your release has been successfully deleted from our system!</div><br />' : '';
					echo ($_GET['edit'] == 'success') ? '<div class="success-message">Your release has been successfully updated!</div><br />' : '';
					echo ($_GET['add'] == 'success') ? '<div class="success-message">Your release has been successfully added!</div><br />' : '';
					echo '<div id="releases-list" class="left">';
					$sth = $dbh->prepare("SELECT * FROM releases ORDER BY dateDeploy DESC");
					$sth->setFetchMode(PDO::FETCH_OBJ);
					$sth->execute();
					while($row = $sth->fetch()){
						($row->dateDeploy == '' || $row->dateDeploy == '0000-00-00 00:00:00') ? $dateDeploy = 'Date not set.' : $dateDeploy = date('F j, Y', strtotime($row->dateDeploy));
						($row->dateCodeCutoff == '' || $row->dateCodeCutoff == '0000-00-00 00:00:00') ? $dateCodeCutoff = 'Date not set.' : $dateCodeCutoff = date('F j, Y', strtotime($row->dateCodeCutoff));
						($row->editedDate == '0000-00-00 00:00:00') ? $editedDate = '' : $editedDate = date('F j, Y @ g:i a', strtotime($row->editedDate));
						echo '<div class="release-item">';
							echo '<div class="row clear">';
								echo '<div class="name left">' . htmlspecialchars($row->name) . '&nbsp;&nbsp;<span class="note">[<a href="releasenotes.php?releaseId=' . $row->id . '">view notes</a>]</span></div>';
								echo '<div class="links right">[<a href="checklist.php?releaseId=' . $row->id . '">view checklist</a>]&nbsp;&nbsp;[<a href="releaseitems.php?releaseId=' . $row->id . '">view release list</a>]</div>';
							echo '</div><div class="row clear">';
								echo '<div class="owner left"><strong>Owner <span class="sep">::</span> </strong>' . $row->owner . '</div>';
								echo '<div class="edit-delete right">[<a href="releaseedit.php?id=' . $row->id . '">Edit Release</a> <strong>/</strong> <a href="releasedelete.php?id=' . $row->id . '" onclick="return confirm(\'Are you sure you want to delete the release: ' . $row->name . '?\');">Delete Release</a>]</div>';
							echo '</div><div class="row clear">';
								echo '<div class="backup-contact left"><strong>Backup Contact <span class="sep">::</span> </strong>' . $row->backupContact . '</div>';
								($row->active == 'true') ? $status = 'active' : $status = 'inactive';
								if($_COOKIE['user'] != 'rpitts' || $status == 'inactive' || date('Y-m-d', strtotime('-90 days')) < $row->dateDeploy){
									echo '<div class="status right"><strong>Status <span class="sep">::</span> </strong>' . $status . '</div>';
								}else if($_COOKIE['user'] == 'rpitts' && date('Y-m-d', strtotime('-90 days')) > $row->dateDeploy && $status == 'active'){
									echo '<div class="status right"><strong>Status <span class="sep">::</span> </strong><span class="red-note">' . $status . '</span></div>';
								}
							echo '</div><div class="row clear">';
								echo '<div class="date-deploy left"><strong>Release Date <span class="sep">::</span> </strong>' . $dateDeploy . '</div>';
								echo '<div class="date-created right"><strong>Date Created <span class="sep">::</span> </strong>' . date('F j, Y @ g:i a', strtotime($row->dateCreated)) . '</div>';
							echo '</div><div class="row clear">';
								echo '<div class="date-code-cutoff left"><strong>Code Cutoff Date <span class="sep">::</span> </strong>' . $dateCodeCutoff . '</div>';
								if(!$editedDate){
									echo '<div class="date-edited right">This release has not been modified.</div>';
								}else{
									echo '<div class="date-edited right"><strong>Last Modified <span class="sep">::</span> </strong>' . $editedDate . '</div>';
								}
							echo '</div>';
							echo '<div class="clear"></div>';
						echo '</div>';
					}
					echo '</div>';
				?>
				<div class="clear"></div>
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>