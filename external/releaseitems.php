<?php
	require_once('../includes/config.php');
	
	$releaseId = $_GET['releaseId'];
	$sth = $dbh->prepare("SELECT * FROM releases WHERE id = ? LIMIT 1");
	$sth->setFetchMode(PDO::FETCH_OBJ);
	$sth->execute(array($_GET['releaseId']));
	while($row = $sth->fetch()){
		$releaseName = $row->name;
		$releaseOwner = $row->owner;
		$releaseBackupContact = $row->backupContact;
		($row->dateDeploy == '0000-00-00 00:00:00' || $row->dateDeploy == '') ? $dateDeploy = 'Date not set.' : $dateDeploy = date('F j, Y', strtotime($row->dateDeploy));
		($row->dateCodeCutoff == '0000-00-00 00:00:00' || $row->dateCodeCutoff == '') ? $dateCodeCutoff = 'Date not set.' : $dateCodeCutoff = date('F j, Y', strtotime($row->dateCodeCutoff));
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Release List for <?php echo $releaseName; ?></title>
	<?php
		require_once('../includes/meta_external.php');
	?>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<div id="main-content">
				<?php
					echo '<h1 class="left">Release List for ' . $releaseName . '</h1>';
					echo '<div class="release-details left">';
						echo '<div class="left">Primary Contact: <span class="accent">' . $releaseOwner . '</span></div>';
						$sth = $dbh->prepare("SELECT IFNULL(COUNT(a.itemId),0) releaseItems,IFNULL(b.projectItems,0) projectItems,IFNULL(c.upgradeItems,0) upgradeItems,IFNULL(d.dataItems,0) dataItems,IFNULL(e.patchItems,0) patchItems FROM releaseitems AS a LEFT OUTER JOIN (SELECT releaseId,COUNT(itemId) projectItems FROM releasespatches WHERE releaseId = :id AND type = 'Project') AS b ON a.releaseId = b.releaseId LEFT OUTER JOIN (SELECT releaseId,COUNT(itemId) upgradeItems FROM releasespatches WHERE releaseId = :id AND type = 'Upgrade') AS c ON a.releaseId = c.releaseId LEFT OUTER JOIN (SELECT releaseId,COUNT(itemId) dataItems FROM releasespatches WHERE releaseId = :id AND type = 'Data') AS d ON a.releaseId = d.releaseId LEFT OUTER JOIN (SELECT releaseId,COUNT(itemId) patchItems FROM releasespatches WHERE releaseId = :id AND type = 'Patch') AS e ON a.releaseId = e.releaseId WHERE a.releaseId = :id");
						$sth->setFetchMode(PDO::FETCH_OBJ);
						$sth->bindParam(':id', $_GET['releaseId']);
						$sth->execute();
						while($row = $sth->fetch()){
							$totalItems = $row->releaseItems + $row->projectItems + $row->upgradeItems + $row->dataItems + $row->patchItems;
							echo '<div class="right">Total Items in Release: <span class="total-items">' . $totalItems . '</span></div>';
							echo '<br />';
							echo '<div class="right"><span class="total-items-breakdown">(bugs: ' . $row->releaseItems . ' | projects: ' . $row->projectItems . ' | upgrades: ' . $row->upgradeItems . ' | data: ' . $row->dataItems . ' | patches: ' . $row->patchItems . ')</span></div>';
						}
						$sth = $dbh->prepare("SELECT ROUND(AVG(riskLevel)) AS riskLevelAvg,ROUND(AVG(comfortLevel)) AS comfortLevelAvg FROM (SELECT riskLevel,comfortLevel FROM releaseitems WHERE releaseId = :id AND riskLevel > '0' AND comfortLevel > '0' UNION ALL SELECT riskLevel,comfortLevel FROM releasespatches WHERE releaseId = :id AND riskLevel > '0' AND comfortLevel > '0') AS c");
						$sth->setFetchMode(PDO::FETCH_OBJ);
						$sth->bindParam(':id', $_GET['releaseId']);
						$sth->execute();
						while($row = $sth->fetch()){
							if($row->riskLevelAvg == '0'){
								echo '<div class="left">Risk Level: <span class="release-level-bad">High Risk</span></div>';
							}else if($row->riskLevelAvg == '1'){
								echo '<div class="left">Risk Level: <span class="release-level-good">Low Risk</span></div>';
							}else if($row->riskLevelAvg == '2'){
								echo '<div class="left">Risk Level: <span class="release-level-mediocre">Medium Risk</span></div>';
							}else if($row->riskLevelAvg == '3'){
								echo '<div class="left">Risk Level: <span class="release-level-bad">High Risk</span></div>';
							}
							echo '<br />';
							if($row->comfortLevelAvg == '0'){
								echo '<div class="left">Comfort Level: <span class="release-level-bad">Low Comfort</span></div>';
							}else if($row->comfortLevelAvg == '3'){
								echo '<div class="left">Comfort Level: <span class="release-level-good">High Comfort</span></div>';
							}else if($row->comfortLevelAvg == '2'){
								echo '<div class="left">Comfort Level: <span class="release-level-mediocre">Medium Comfort</span></div>';
							}else if($row->comfortLevelAvg == '1'){
								echo '<div class="left">Comfort Level: <span class="release-level-bad">Low Comfort</span></div>';
							}
							echo '<br />';
						}
						echo '<div class="right"><a href="excelexport.php?releaseId=' . $_GET['releaseId'] . '">Export List to CSV</a></div>';
						echo '<div class="left">Release Date: <span class="accent">' . $dateDeploy . '</span></div>';
						echo '<br />';
						echo '<div class="left">Code Cutoff Date: <span class="accent">' . $dateCodeCutoff . '</span></div>';
					echo '</div>';
					echo '<div id="tools" class="left">';
						echo '<h2>Products Affected</h2>';
						$sth = $dbh->prepare("SELECT * FROM tools WHERE releaseId = ? AND active = 'true' ORDER BY rowId ASC");
						$sth->setFetchMode(PDO::FETCH_OBJ);
						$sth->execute(array($_GET['releaseId']));
						while($row = $sth->fetch()){
							echo '<img src="../images/' . $row->label . '_' . $row->value . '.png" class="tool" width="85" /></a>';
						}
					echo '</div>';
					echo '<div id="release-list-container" class="left">';
						$itemTypes = array('Release','Project','Upgrade','Data','Patch');
						foreach($itemTypes as $k => $v){
							echo '<div class="release-items-list">';
								echo '<h2>';
									echo $v . ' Items';
								echo '</h2>';
								($v == 'Release') ? $sql = "SELECT i.*,b.affectedBrowsers,d.affectedDesigns FROM releaseitems AS i LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedBrowsers FROM affectedbrowsers GROUP BY itemId) AS b ON i.itemId = b.itemId LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedDesigns FROM affecteddesigns GROUP BY itemId) AS d ON i.itemId = d.itemId WHERE i.releaseId = ? ORDER BY i.itemId ASC" : $sql = "SELECT p.*,b.affectedBrowsers,d.affectedDesigns FROM releasespatches AS p LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedBrowsers FROM affectedbrowsers GROUP BY itemId) AS b ON p.itemId = b.itemId LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedDesigns FROM affecteddesigns GROUP BY itemId) AS d ON p.itemId = d.itemId WHERE p.releaseId = ? AND type = '" . $v . "' ORDER BY p.itemId ASC";
								$sth = $dbh->prepare($sql);
								$sth->setFetchMode(PDO::FETCH_OBJ);
								$sth->execute(array($_GET['releaseId']));
								if($sth->rowCount() < 1){
									echo '<div class="no-results">';
										echo 'There are no ' . strtolower($v) . ' items in the system for this release. Use the link above to add ' . strtolower($v) . ' items to this release.';
									echo '</div>';
								}else{
									while($row = $sth->fetch()){
										if($row->referenceType == 'FogBugz'){
											$referenceItemUrl = 'https://clickmotive.fogbugz.com/default.asp?' . $row->referenceId;
										}else if($row->referenceType == 'VersionOne'){
											$referenceItemUrl = 'https://www10.v1host.com/ClickMotive/Search.mvc/advanced?q=' . $row->referenceId;
										}else if($row->referenceType == 'ZenDesk'){
											$referenceItemUrl = 'http://clickmotive.zendesk.com/tickets/' . $row->referenceId;
										}
										echo '<div class="release-item">';
											echo '<div class="release-title">';
												echo '<a href="' . $referenceItemUrl . '" class="no-link" target="_blank" title="View Defect"><span class="accent">' . $row->referenceId . '</span> - ' . $row->title . '</a>';
											echo '</div>';
											echo '<div class="risk-comfort-levels">';
												if($row->riskLevel == '0'){
													echo 'Risk: <span class="">N/A</span>';
												}else if($row->riskLevel == '1'){
													echo 'Risk: <span class="release-level-good">Low</span>';
												}else if($row->riskLevel == '2'){
													echo 'Risk: <span class="release-level-mediocre">Medium</span>';
												}else if($row->riskLevel == '3'){
													echo 'Risk: <span class="release-level-bad">High</span>';
												}
												echo '&nbsp;&nbsp;|&nbsp;&nbsp;';
												if($row->comfortLevel == '0'){
													echo 'Comfort: <span class="">N/A</span>';
												}else if($row->comfortLevel == '1'){
													echo 'Comfort: <span class="release-level-bad">Low</span>';
												}else if($row->comfortLevel == '2'){
													echo 'Comfort: <span class="release-level-mediocre">Medium</span>';
												}else if($row->comfortLevel == '3'){
													echo 'Comfort: <span class="release-level-good">High</span>';
												}
											echo '</div>';
											echo '<div class="data-changes">';
												echo '<span class="accent"><strong>Data Change?</strong></span> ';
												echo ($row->dataChange == 1) ? 'Yes' : 'No';
											echo '</div>';
											echo '<div class="config-changes">';
												echo '<span class="accent"><strong>Configuration File Change?</strong></span> ';
												echo ($row->configChange == 1) ? 'Yes' : 'No';
											echo '</div>';
											echo '<div class="affected-items">';
												echo '<span class="accent"><strong>Affected Product:</strong></span> ';
												echo ($row->affectedProduct == '') ? 'N/A' : $row->affectedProduct;
												echo '<br />';
												echo '<span class="accent"><strong>Affected Browser(s):</strong></span> ';
												echo $row->affectedBrowsers;
												echo '<br />';
												echo '<span class="accent"><strong>Affected Design(s):</strong></span> ';
												echo $row->affectedDesigns;
											echo '</div>';
											echo '<div class="release-item-comment">';
												echo '<span class="accent"><strong>Comment:</strong></span> ';
												echo ($row->comments == '') ? 'none' : $row->comments;
											echo '</div>';
										echo '</div>';
									}
								}
							echo '</div>';
						}
					echo '</div>';
					echo '<div class="clear"></div>';
				?>
			</div>
		</div>
		<?php
			require_once('footer.php');
			require_once('../includes/closeconn.php');
		?>
	</div>
</body>
</html>