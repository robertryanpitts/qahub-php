<div id="nav-wrapper" class="left">
	<ul class="menu left">
		<li>
			<span>Releases</span>
		</li>
		<div class="sub-menu">
			<ul class="sub-menu-list left">
				<li>
					<a href="checklists.php">
						<span>
							View Checklists
						</span>
					</a>
				</li>
				<li>
					<a href="releaselists.php">
						<span>
							View Release Lists
						</span>
					</a>
				</li>
				<li>
					<a href="releases.php">
						<span>
							Manage Releases
						</span>
					</a>
				</li>
				<li>
					<a href="search.php">
						<span>
							Live Search
						</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="clear"></div>
		<li>
			<span>Blog</span>
		</li>
		<div class="sub-menu">
			<ul class="sub-menu-list left">
				<li>
					<a href="blog/">
						<span>
							View Blog
						</span>
					</a>
				</li>
				<li>
					<a href="blog/wp-login.php">
						<span>
							Blog Login
						</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="clear"></div>
		<li>
			<span>Essential Tools</span>
		</li>
		<div class="sub-menu">
			<ul class="sub-menu-list left">
				<li>
					<a href="http://confluence.clickmotive.com/display/qa/Home" target="_blank">
						<span>
							QA Confluence Page
						</span>
					</a>
				</li>
				<li>
					<a href="testboard.php">
						<span>
							Test Board
						</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="clear"></div>
		<li>
			<span>Users</span>
		</li>
		<div class="sub-menu">
			<ul class="sub-menu-list left">
				<li>
					<a href="users.php">
						<span>
							Manage Users
						</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="clear"></div>
		<li>
			<span>Other</span>
		</li>
		<div class="sub-menu">
			<ul class="sub-menu-list left">
				<li>
					<a href="home.php">
						<span>
							Home
						</span>
					</a>
				</li>
			</ul>
		</div>
	</ul>
</div>