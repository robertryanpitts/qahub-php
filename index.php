<?php
	(isset($_COOKIE['user'])) ? header("location:home.php") : '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Login</title>
	<?php
		require_once('includes/meta_login.php');
	?>
	<script type="text/javascript">
		$(document).ready(function(){
			$("input#user").focus();
		});
	</script>
</head>
<body>
	<div id="login-wrapper">
		<div class="title-message left">
			<span class="login-title">
				QA Hub
			</span>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="login-form">
			<?php
				require_once 'includes/config.php';
				if($_GET['auth'] == 'error'){
			?>
			<div class="message left">
				<span class="error-message">Invalid Username/Password. Please try again!</span>
			</div>
			<?php
				}else if($_GET['auth'] == 'login'){
			?>
			<div class="message left">
				<span class="error-message">You must be logged in to view this page!</span>
			</div>
			<?php
				}else if($_GET['active'] == 'false'){
			?>
			<div class="message left">
				<span class="error-message">Your account is not active!</span>
			</div>
			<?php
				}else if($_GET['logout'] == 'success'){
			?>
			<div class="message left">
				<span class="success-message">You have been successfully logged out!</span>
			</div>
			<?php
				}else if($_GET['sent'] == 'success'){
			?>
			<div class="message left">
				<span class="success-message">Your login info has been sent to your email!</span>
			</div>
			<?php
				}
			?>
			<div class="clear"></div>
			<form name="login_form" method="post" action="authenticate.php">
				<div class="form-row">
					Username:&nbsp;&nbsp;&nbsp;<input type="text" id="user" name="User" />
				</div>
				<div class="form-row">
					Password:&nbsp;&nbsp;&nbsp;<input type="password" id="pass" name="Pass" />
				</div>
				<div class="form-row">
					<label>
						<input type="checkbox" name="remember" />
						Remember Me?
					</label>
				</div>
				<div class="form-row form-btns">
					<input type="text" id="bot-catcher" name="BotCatcher" />
					<input class="input-btn" type="submit" name="Login" id="login" value="Login" />
					<div class="get-credentials">
						<a href="emailuserinfo.php">Forget your username and/or password?</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>