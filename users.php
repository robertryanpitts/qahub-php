<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Users Manager</title>
	<?php
		require_once('includes/meta.php');
	?>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<h1>
					<strong>Manage Users</strong>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="useradd.php">Add a User</a>
				</h1>
				<br />
				<?php
					echo ($_GET['delete'] == 'success') ? '<div class="success-message">' . $_GET['user'] . 'has been successfully deleted from our system!</div><br />' : '';
					echo ($_GET['edit'] == 'success') ? '<div class="success-message">' . $_GET['user'] . 'has been successfully updated!</div><br />' : '';
					echo ($_GET['add'] == 'success') ? '<div class="success-message">' . $_GET['user'] . 'has been successfully added! An email has been sent to ' . $_GET['email'] . 'with login instructions.</div><br />' : '';
					$sth = $dbh->prepare("SELECT * FROM users ORDER BY username ASC");
					$sth->setFetchMode(PDO::FETCH_OBJ);
					$sth->execute();
					echo '<div id="users-list" class="left">';
					while($row = $sth->fetch()){
						$editedDate = date('F j, Y @ g:i a', strtotime($row->editedDate));
						($row->dateLastLoggedIn == '0000-00-00 00:00:00' || $row->dateLastLoggedIn == '') ? $dateLastLoggedIn = '' : $dateLastLoggedIn = date('F j, Y @ g:i a', strtotime($row->dateLastLoggedIn));
						echo '<div class="user-item">';
							echo '<div class="row">';
								echo '<div class="username left">' . $row->username . '</div>';
								echo '<div class="nickname left">(' . $row->nickname . ')</div>';
								echo ($row->isAdmin == 'true') ? '<div class="admin right">Administrator</div>' : '';
							echo '</div><div class="row">';
								echo '<div class="name left"><strong>Full Name&nbsp;&nbsp;<span class="sep">::</span>&nbsp;&nbsp;</strong>' . $row->firstName . ' ' . $row->lastName . '</div>';
								echo (!$dateLastLoggedIn) ? '<div class="date-logged-in right">' . $row->nickname . ' has yet to log in!</div>' : '<div class="date-logged-in right"><strong>Last Logged In&nbsp;&nbsp;<span class="sep">::</span>&nbsp;&nbsp;</strong>' . $dateLastLoggedIn . '</div>';
							echo '</div><div class="row">';
								echo '<div class="email left"><strong>Email&nbsp;&nbsp;<span class="sep">::</span>&nbsp;&nbsp;</strong>' . $row->email . '</div>';
								echo '<div class="edited-date right"><strong>Last Modified&nbsp;&nbsp;<span class="sep">::</span>&nbsp;&nbsp;</strong>' . $editedDate . '</div>';
							echo '</div><div class="row">';
								($row->active == 'true') ? $status = 'active' : $status = 'inactive';
								echo '<div class="status left"><strong>Status&nbsp;&nbsp;<span class="sep">::</span>&nbsp;&nbsp;</strong>' . $status . '</div>';
								echo ($_COOKIE['admin'] == 'true'  || $row->username == $_COOKIE['user']) ? '<div class="edit-user right"><strong>[</strong><a href="useredit.php?id=' . $row->id . '"> Edit User </a><strong>]</strong></div>' : '';
							echo '</div>';
							echo '<div class="clear"></div>';
						echo '</div>';
						}
						echo '</div>';
					?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>