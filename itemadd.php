<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	$sth = $dbh->prepare("SELECT * FROM releases WHERE id = ? LIMIT 1");
	$sth->setFetchMode(PDO::FETCH_OBJ);
	$sth->execute(array($_GET['releaseId']));
	while($row = $sth->fetch()){
		$releaseName = $row->name;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Add A Release Item in <?php echo $releaseName; ?></title>
	<?php
		require_once('includes/meta.php');
	?>
	<script type="text/javascript">
		$(document).ready(function(){
			checkCheckboxes();
		});
		var checkCheckboxes = function checkCheckboxes(){
			$('input:checked').parent().addClass('checkedBox');
			$('input:not(:checked)').parent().removeClass('checkedBox');
		}
	</script>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<h1>
					<strong>Add an Item in <?php echo $releaseName; ?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="releaseitems.php?releaseId=<?php echo $_GET['releaseId']; ?>" style="font-size:18px;">Back to the Release List</a>
				</h1>
				<br />
				<form name="add_item" method="post" action="itempostadd.php">
					<div class="form-row">
						Reference:
						<br />
						<select name="referenceType">
							<option value="FogBugz">FogBugz</option>
							<option value="VersionOne" selected="selected">VersionOne</option>
							<option value="ZenDesk">ZenDesk</option>
						</select>&nbsp;&nbsp;ID #
						<input type="text" name="referenceId" maxlength="100" />
					</div>
					<div class="form-row">
						Case Title:
						<br />
						<input class="large-input" type="text" name="title" maxlength="300" />
					</div>
					<div class="form-row">
						Data Change?
						<br />
						<input type='radio' name='dataChange' value='1' />
						<strong>Yes</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='radio' name='dataChange' value='0' />
						<strong>No</strong>
					</div>
					<div class="form-row">
						Configuration File Change?
						<br />
						<input type='radio' name='configChange' value='1' />
						<strong>Yes</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='radio' name='configChange' value='0' />
						<strong>No</strong>
					</div>
					<div class="form-row">
						Affected Browsers:
						<br />
						<span class="tooltip">
							<img src="images/tooltip.png" /> check all that apply
						</span>
						<br />
						<table id="affedcted-browsers-grid">
							<tr>
								<?php
									$sth = $dbh->prepare("SELECT * FROM affectedbrowsersdata WHERE active = true ORDER BY label ASC");
									$sth->setFetchMode(PDO::FETCH_OBJ);
									$sth->execute();
									$i = 1;
									while($row = $sth->fetch()){
										if($i == 1){
											echo '<td valign="top">';
												echo '<label><input type="checkbox" name="affectedbrowsers[]" value="' . $row->browser . '" onclick="checkCheckboxes();" />' . $row->label . '</label>';
												echo '<br />';
											$i = $i+1;
										}else if($i == 3){
												echo '<label><input type="checkbox" name="affectedbrowsers[]" value="' . $row->browser . '" onclick="checkCheckboxes();" />' . $row->label . '</label>';
											echo '</td>';
											$i = 1;
										}else{
											echo '<label><input type="checkbox" name="affectedbrowsers[]" value="' . $row->browser . '" onclick="checkCheckboxes();" />' . $row->label . '</label>';
											echo '<br />';
											$i = $i+1;
										}
									}
								?>
							</tr>
						</table>
					</div>
					<div class="form-row">
						Affected Designs:
						<br />
						<span class="tooltip">
							<img src="images/tooltip.png" /> check all that apply
						</span>
						<br />
						<table id="affected-designs-grid" width="100%">
							<tr>
								<?php
									$sth = $dbh->prepare("SELECT * FROM affecteddesignsdata WHERE active = true ORDER BY label ASC");
									$sth->setFetchMode(PDO::FETCH_OBJ);
									$sth->execute();
									$i = 1;
									while($row = $sth->fetch()){
										if($i == 1){
											echo '<td width="20%" valign="top">';
												echo '<label><input type="checkbox" name="affecteddesigns[]" value="' . $row->design . '" onclick="checkCheckboxes();" />' . $row->label . '</label>';
												echo '<br />';
											$i = $i+1;
										}else if($i == 15){
												echo '<label><input type="checkbox" name="affecteddesigns[]" value="' . $row->design . '" onclick="checkCheckboxes();" />' . $row->label . '</label>';
											echo '</td>';
											$i = 1;
										}else{
											echo '<label><input type="checkbox" name="affecteddesigns[]" value="' . $row->design . '" onclick="checkCheckboxes();" />' . $row->label . '</label>';
											echo '<br />';
											$i = $i+1;
										}
									}
								?>
							</tr>
						</table>
					</div>
					<div class="form-row">
						Affected Product:
						<br />
						<select name="affectedProduct">
							<option value="N/A">N/A</option>
							<option value="Asset Server">Asset Server</option>
							<option value="FastLane">FastLane</option>
							<option value="Fusion Website">Fusion Website</option>
							<option value="Inventory Process">Inventory Process</option>
							<option value="New Relic">New Relic</option>
						</select>
					</div>
					<div class="form-row">
						Comfort Level:
						<br />
						<select name="comfortLevel">
							<option value="0"></option>
							<option value="1" class="release-level-bad">Low</option>
							<option value="2" class="release-level-mediocre">Medium</option>
							<option value="3" class="release-level-good">High</option>
						</select>
					</div>
					<div class="form-row">
						Risk Level:
						<br />
						<select name="riskLevel">
							<option value="0"></option>
							<option value="1" class="release-level-good">Low</option>
							<option value="2" class="release-level-mediocre">Medium</option>
							<option value="3" class="release-level-bad">High</option>
						</select>
					</div>
					<div class="form-row">
						Comments:
						<br />
						<input class="large-input" type="text" name="comments" />
					</div>
					<div class="form-row">
						<input name="releaseId" type="hidden" value="<?php echo $_GET['releaseId']; ?>" />
						<input class="input-btn" type="submit" name="add" value="Add" />
						<input class="input-btn btn-cancel" type="button" name="cancel" value="Cancel" onclick="history.go(-1)" />
					</div>
				</form>
				<br />
			</div>
			<?php
				require_once('includes/closeconn.php');
			?>
		</div>
	</div>
</body>
</html>