<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Flex URLs</title>
	<?php
		require_once('includes/meta.php');
	?>
</head>
<body>
	<div id="flex-urls">
		<h2>Flex URLs</h2>
		<strong>Flex Home Page</strong><br />
		<a href="http://www.clickmotiveflexdev.com/pages/ford/sites/v2/default.aspx?c=27648" target="_blank">http://www.clickmotiveflexdev.com/pages/ford/sites/v2/default.aspx?c=27648</a><br />
		<a href="http://www.clickmotiveflexstage.com/pages/ford/sites/v2/default.aspx?c=27648" target="_blank">http://www.clickmotiveflexstage.com/pages/ford/sites/v2/default.aspx?c=27648</a><br />
		<a href="http://www.clickmotiveflex.com/pages/ford/sites/v2/default.aspx?c=27648" target="_blank">http://www.clickmotiveflex.com/pages/ford/sites/v2/default.aspx?c=27648</a><br />
		<br /><br />
		<strong>Flex Brochures</strong><br />
		<a href="http://www.clickmotiveflexdev.com/pages/ford/sites/v2fordModelCatalog.html" target="_blank">http://www.clickmotiveflexdev.com/pages/ford/sites/v2fordModelCatalog.html</a><br />
		<a href="http://www.clickmotiveflexstage.com/pages/ford/sites/v2/fordModelCatalog.html" target="_blank">http://www.clickmotiveflexstage.com/pages/ford/sites/v2/fordModelCatalog.html</a><br />
		<a href="http://www.clickmotiveflexdev.com/pages/ford/sites/v2/modelCatalog.aspx" target="_blank">http://www.clickmotiveflexdev.com/pages/ford/sites/v2/modelCatalog.aspx</a><br />
	</div>
</body>
</html>