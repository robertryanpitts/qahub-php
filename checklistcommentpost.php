<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	if(isset($_POST['CommentAdd'])){
		if($_POST['comment'] != ''){
			$sth = $dbh->prepare("INSERT INTO comments (tableName, releaseId, rowId, comment, editedBy, editedDate) VALUES (?, ?, ?, ?, ?, ?)");
			$sth->execute(array($_POST['tableName'], $_POST['releaseId'], $_POST['rowId'], stripslashes($_POST['comment']), $_COOKIE['user'], date('Y-m-d  H:i:s')));
		}
		if($_POST['tableName'] != 'checklistproject'){
			$sql = "UPDATE " . $_POST['tableName'] . " SET editedBy = ?, editedDate = ? WHERE rowId = ?";
			$sth = $dbh->prepare($sql);
			$sth->execute(array($_COOKIE['user'], date('Y-m-d  H:i:s'), $_POST['rowId']));
		}
	}
	if(isset($_POST['CommentEdit'])){
		if($_POST['comment'] != ''){
			$sth = $dbh->prepare("UPDATE comments SET comment = ?, editedBy = ?, editedDate = ? WHERE commentId = ?");
			$sth->execute(array(stripslashes($_POST['comment']), $_COOKIE['user'], date('Y-m-d  H:i:s'), $_POST['commentId']));
		}
		if($_POST['tableName'] != 'checklistproject'){
			$sql = "UPDATE " . $_POST['tableName'] . " SET editedBy = ?, editedDate = ? WHERE rowId = ?";
			$sth = $dbh->prepare($sql);
			$sth->execute(array($_COOKIE['user'], date('Y-m-d  H:i:s'), $_POST['rowId']));
		}
	}
	$sth = $dbh->prepare("UPDATE releases SET modifiedBy = ?, editedDate = ? WHERE id = ?");
	$sth->execute(array($_COOKIE['user'], date('Y-m-d  H:i:s'), $_POST['releaseId']));
	require_once('includes/meta.php');
	require_once('includes/closeconn.php');
?>
<script type="text/javascript">
	$(document).ready(function(){
		parent.$.fancybox.close();
	});
</script>