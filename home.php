<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Welcome</title>
	<?php
		require_once('includes/meta.php');
	?>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<h1>Welcome to the QA Hub!</h1>
				<div class="recently-updated-items-list">
					<h2>Last 5 Updated Release Items - <span>(not including data, patch, project, upgrade items)</span></h2>
					<?php
						$sth = $dbh->prepare("SELECT r.name AS releaseName,i.itemId,i.referenceType,i.referenceId,i.title,i.affectedProduct,i.comfortLevel,i.riskLevel,i.comments,b.affectedBrowsers,d.affectedDesigns FROM releaseitems AS i LEFT OUTER JOIN releases AS r on r.id = i.releaseId LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedBrowsers FROM affectedbrowsers WHERE type = 'Item' GROUP BY itemId) AS b ON i.itemId = b.itemId LEFT OUTER JOIN (SELECT itemId, GROUP_CONCAT(' ', label) affectedDesigns FROM affecteddesigns WHERE type = 'Item' GROUP BY itemId) AS d ON i.itemId = d.itemId WHERE i.releaseId = 34 ORDER BY i.editedDate DESC LIMIT 5");
						$sth->setFetchMode(PDO::FETCH_OBJ);
						$sth->execute();
						while($row = $sth->fetch()){
							$itemId = $row->itemId;
							if($row->referenceType == 'FogBugz'){
								$referenceItemUrl = 'https://clickmotive.fogbugz.com/default.asp?' . $row->referenceId;
							}else if($row->referenceType == 'VersionOne'){
								$referenceItemUrl = 'https://www10.v1host.com/ClickMotive/Search.mvc/advanced?q=' . $row->referenceId;
							}else if($row->referenceType == 'ZenDesk'){
								$referenceItemUrl = 'http://clickmotive.zendesk.com/tickets/' . $row->referenceId;
							}
							echo '<div class="updated-item">';
								echo '<div class="updated-title">' . $row->releaseName . ' :: <a href="' . $referenceItemUrl . '" class="no-link" target="_blank" title="View Defect"><span class="accent">' . $row->referenceId . '</span> - ';
									echo ($row->dataChange == 1 || $row->configChange == 1) ? '<span class="warning">' . $row->title . '</span>' : $row->title;
								echo '</a></div>';
								if($row->comfortLevel == '0'){
									$comfortLevelItem = 'N/A';
									$comfortColor = '';
								}else if($row->comfortLevel == '1'){
									$comfortLevelItem = 'Low';
									$comfortColor = 'release-level-bad';
								}else if($row->comfortLevel == '2'){
									$comfortLevelItem = 'Medium';
									$comfortColor = 'release-level-mediocre';
								}else if($row->comfortLevel == '3'){
									$comfortLevelItem = 'High';
									$comfortColor = 'release-level-good';
								}
								if($row->riskLevel == '0'){
									$riskLevelItem = 'N/A';
									$riskColor = '';
								}else if($row->riskLevel == '1'){
									$riskLevelItem = 'Low';
									$riskColor = 'release-level-good';
								}else if($row->riskLevel == '2'){
									$riskLevelItem = 'Medium';
									$riskColor = 'release-level-mediocre';
								}else if($row->riskLevel == '3'){
									$riskLevelItem = 'High';
									$riskColor = 'release-level-bad';
								}
								echo '<div class="updated-item-risk-comfort-levels">Risk: <span class="' . $riskColor . '">' . $riskLevelItem . '</span>&nbsp;&nbsp;|&nbsp;&nbsp;Comfort: <span class="' . $comfortColor . '">' . $comfortLevelItem . '</span></div>';
								echo '<div class="updated-item-data-changes">';
									echo '<span class="accent"><strong>Data Change?</strong></span> ';
									echo ($row->dataChange == 1) ? 'Yes' : 'No';
								echo '</div>';
								echo '<div class="updated-item-config-changes">';
									echo '<span class="accent"><strong>Configuration File Change?</strong></span> ';
									echo ($row->configChange == 1) ? 'Yes' : 'No';
								echo '</div>';
								echo '<div class="updated-item-affected-items">';
									$itemAffectedProduct = ($row->affectedProduct == '') ? 'N/A' : $row->affectedProduct;
									echo '<span class="accent"><strong>Affected Product:</strong></span> ' . $itemAffectedProduct . '<br />';
									echo '<span class="accent"><strong>Affected Browser(s):</strong></span> ';
									echo ($row->affectedBrowsers == '') ? 'N/A' : $row->affectedBrowsers;
									echo '<br />';
									echo '<span class="accent"><strong>Affected Design(s):</strong></span> ';
									echo ($row->affectedDesigns == '') ? 'N/A' : $row->affectedDesigns;
								echo '</div>';
								echo '<div class="updated-item-comment">';
									echo '<span class="accent"><strong>Comment:</strong></span> ';
									echo ($row->comments == '') ? 'none' : $row->comments;
								echo '</div>';
							echo '</div>';
						}
					?>
				</div>
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>