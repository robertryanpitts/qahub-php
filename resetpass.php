<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Reset Password</title>
	<?php
		require_once('includes/meta.php');
	?>
	<script type="text/javascript" src="js/password.validator.js"></script>
</head>
<body>
	<div class="reset-old-password">
		<?php
			if($_GET['edit'] == 'true'){
				$editedDate = date('Y-m-d  H:i:s');
				$username = $_GET['user'];
		?>
		<h1>
			<strong>Reset Password</strong>
		</h1>
		<form name="reset_pass" method="post" action="resetpasspost.php">
			<div class="form-row">
				<div class="message"></div>
			</div>
				<div class="form-row">
				<span class="reset-password-label">Password: </span>
				<input type="password" class="new-password password" name="password" />
			</div>
			<div class="form-row">
				<input name="editedDate" type="hidden" value="<?php echo $editedDate; ?>" />
				<input name="username" type="hidden" value="<?php echo $username; ?>" />
				<input class="input-btn reset-password-btn" type="submit" name="reset_pass" value="Reset Password" />
			</div>
		</form>
		<?php
				require_once('includes/closeconn.php');
			}else if($_GET['reset']=="success"){
		?>
		<h1 class="reset-success">Your password has been successfully reset!</h1>
		<?php
			}
		?>
	</div>
</body>
</html>