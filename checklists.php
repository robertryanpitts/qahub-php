<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Release Checklists</title>
	<?php
		require_once('includes/meta.php');
	?>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<h1>Release Checklists</h1>
				<div id="container">
					<h2>Current Releases:</h2>
					<?php
						$sth = $dbh->prepare("SELECT * FROM releases WHERE active = 'true' AND addtime(dateDeploy, '6:00:00') > now() ORDER BY dateDeploy DESC");
						$sth->setFetchMode(PDO::FETCH_OBJ);
						$sth->execute();
						if($sth->rowCount() < 1){
							echo '<div class="releases">';
								echo 'There are no current releases to display.';
							echo '</div>';
						}else{
							while($row = $sth->fetch()){
								($row->editedDate == '0000-00-00 00:00:00') ? $editedDate = '' : $editedDate = date('F j, Y @ g:i a', strtotime($row->editedDate));
								if(!$editedDate){
									echo '<a href="checklist.php?releaseId=' . $row->id . '">';
										echo '<div class="releases">';
											echo '<span class="checklist-title">';
												echo $row->name . '<br />';
											echo '</span><span class="checklist-release-date">';
												echo 'This release is scheduled for ' . date('F j, Y', strtotime($row->dateDeploy)) . '<br />';
											echo '</span><span class="checklist-link">';
												echo '[click to view checklist]';
											echo '</span>';
										echo '</div>';
									echo '</a>';
								}else{
									echo '<a href="checklist.php?releaseId=' . $row->id . '">';
										echo '<div class="releases">';
											echo '<span class="checklist-title">';
												echo $row->name;
											echo '</span><span class="checklist-edited-date">';
												echo ' (last updated on ' . $editedDate . ' by ' . $row->modifiedBy . ')<br />';
											echo '</span><span class="checklist-release-date">';
												echo 'This release is scheduled for ' . date('F j, Y', strtotime($row->dateDeploy)) . '<br />';
											echo '</span><span class="checklist-link">';
												echo '[click to view checklist]';
											echo '</span>';
										echo '</div>';
									echo '</a>';
								}
							}
						}
					?>
					<br />
					<h2>Past Releases: <span class="title-small-txt">(up to 3 months ago)</span></h2>
					<?php
						$sth = $dbh->prepare("SELECT * FROM releases WHERE active = 'true' AND addtime(dateDeploy, '6:00:00') < now() ORDER BY dateDeploy DESC");
						$sth->setFetchMode(PDO::FETCH_OBJ);
						$sth->execute();
						if($sth->rowCount() < 1){
							echo '<div class="releases">';
								echo 'There are no past releases to display.';
							echo '</div>';
						}else{
							while($row = $sth->fetch()){
								($row->editedDate == '0000-00-00 00:00:00') ? $editedDate = '' : $editedDate = date('F j, Y @ g:i a', strtotime($row->editedDate));
								if(!$editedDate){
									echo '<a href="checklist.php?releaseId=' . $row->id . '">';
										echo '<div class="releases">';
											echo '<span class="checklist-title">';
												echo $row->name . '<br />';
											echo '</span><span class="checklist-release-date">';
												echo 'This release is scheduled for ' . date('F j, Y', strtotime($row->dateDeploy)) . '<br />';
											echo '</span><span class="checklist-link">';
												echo '[click to view checklist]';
											echo '</span>';
										echo '</div>';
									echo '</a>';
								}else{
									echo '<a href="checklist.php?releaseId=' . $row->id . '">';
										echo '<div class="releases">';
											echo '<span class="checklist-title">';
												echo $row->name;
											echo '</span><span class="checklist-edited-date">';
												echo ' (last updated on ' . $editedDate . ' by ' . $row->modifiedBy . ')<br />';
											echo '</span><span class="checklist-release-date">';
												echo 'This release is scheduled for ' . date('F j, Y', strtotime($row->dateDeploy)) . '<br />';
											echo '</span><span class="checklist-link">';
												echo '[click to view checklist]';
											echo '</span>';
										echo '</div>';
									echo '</a>';
								}
							}
						}
					?>
				</div>
			</div>
		</div>
		<?php
			require_once('footer.php');
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>