<?php
	require_once('includes/config.php');
	require_once('authorize.php');

	$sth = $dbh->prepare("SELECT nickname FROM users WHERE id = ? LIMIT 1");
	$sth->setFetchMode(PDO::FETCH_OBJ);
	$sth->execute(array($_GET['id']));
	while($row = $sth->fetch()){
		$nickname = $row->nickname;
	}
	$sthUser = $dbh->prepare("DELETE FROM users WHERE id = ? LIMIT 1");
	$sthUser->execute(array($_GET['id']));
	require_once('includes/closeconn.php');
	header('location:users.php?delete=success&user=' . $nickname);
?>