<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Error Handling</title>
	<?php
		require_once('includes/meta.php');
	?>
</head>
<body>
	<div id="error-handling">
		<h2>Error Handling Instructions</h2>
		<strong>404 Error:</strong>
		<br />
		Type in {customerId}.{environment}.com/Fail.aspx
		<br />
		(ex, 27804.clickmotivefusion.com/Fail.aspx)
		<br /><br />
		<strong>500 Error:</strong>
		<br />
		Type in {customerId}.{environment}.com/Testing/ForceError.aspx
		<br />
		(ex, 27804.clickmotivefusion.com/Testing/ForceError.aspx)
	</div>
</body>
</html>