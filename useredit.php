<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>QA Hub | Admin | Edit User</title>
	<?php
		require_once('includes/meta.php');
	?>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.reset-password').fancybox({
				autoSize: false,
				height: 165,
				width: 400,
				type: 'iframe',
				padding: 0
			});
		});
	</script>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
				$sth = $dbh->prepare("SELECT * FROM users WHERE id = ? LIMIT 1");
				$sth->setFetchMode(PDO::FETCH_OBJ);
				$sth->execute(array($_GET['id']));
				while($row = $sth->fetch()){
					$id = $row->id;
					$username = $row->username;
					$firstName = $row->firstName;
					$lastName = $row->lastName;
					$nickname = $row->nickname;
					$email = $row->email;
					$selectedColorScheme = $row->selectedColorScheme;
					$admin = $row->isAdmin;
					$active = $row->active;
					$editedDate = date('Y-m-d  H:i:s');
				}
			?>
			<div id="main-content">
				<div class="left user-heading">
					<h1 class="left">
						<strong>User Editor</strong>&nbsp;&nbsp;|&nbsp;&nbsp;
						<a href="users.php">Back to User Manager</a>
					</h1>
					<h3 class="right">
						<a href="resetpass.php?edit=true&user=<?php echo $username; ?>" class="input-btn reset-password">Reset Password</a>
						<a href="userdelete.php?id=<?php echo $id; ?>" class="input-btn" onclick="return confirm('Are you sure you want to delete this user?');">Delete User</a>
					</h3>
				</div>
				<form name="edit_user" method="post" action="userpostedit.php">
					<div class="form-row">
						Username:
						<br />
						<input class="large-input" type="text" name="username" value="<?php echo $username; ?>" maxlength="25" />
					</div>
					<div class="form-row">
						First Name:
						<br />
						<input type="text" name="firstName" value="<?php echo $firstName; ?>" maxlength="45" />
					</div>
					<div class="form-row">
						Last Name:
						<br />
						<input type="text" name="lastName" value="<?php echo $lastName; ?>" maxlength="45" />
					</div>
					<div class="form-row">
						Nickname:
						<br />
						<input class="large-input" type="text" name="nickname" value="<?php echo $nickname; ?>" maxlength="50" />
					</div>
					<div class="form-row">
						Email:
						<br />
						<input class="large-input" type="text" name="email" value="<?php echo $email ?>" maxlength="40" />
					</div>
					<div class="form-row">
						Set Color Scheme:
						<br />
						<select name="colorScheme">
							<?php
								$sthColor = $dbh->prepare("SELECT * FROM colorschemes WHERE active = 1 ORDER BY colorSchemeName ASC");
								$sthColor->setFetchMode(PDO::FETCH_OBJ);
								$sthColor->execute();
								while($row = $sthColor->fetch()){
									echo ($selectedColorScheme == $row->colorSchemeName) ? '<option value="' . $row->colorSchemeName . '" selected="selected">' . $row->friendlyName . '</option>' : '<option value="' . $row->colorSchemeName . '">' . $row->friendlyName . '</option>';
								}
							?>
						</select>
					</div>
					<?php
						if($_COOKIE['admin'] == 'true'){
					?>
					<div class="form-row">
						Make Administrator?
						<br />
						<input type='radio' name='admin' value='true' <?php echo ($admin == 'true') ? 'checked="checked"' : ''; ?> />
						<strong>Yes</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='radio' name='admin' value='false' <?php echo ($admin == 'false') ? 'checked="checked"' : ''; ?> />
						<strong>No</strong>
					</div>
					<div class="form-row">
						Active:
						<br />
						<span style="font-style:italic;">
							<img src="images/tooltip.png" /> inactive users will not be able to log in
						</span>
						<br />
						<input type='radio' name='active' value='true' <?php echo ($active == 'true') ? 'checked="checked"' : ''; ?> />
						<strong>Active</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type='radio' name='active' value='false' <?php echo ($active == 'false') ? 'checked="checked"' : ''; ?> />
						<strong>Inactive</strong>
					</div>
					<?php
						}else{
							echo '<input type="hidden" name="admin" value="' . $admin . '" />';
							echo '<input type="hidden" name="active" value="' . $active . '" />';
						}
					?>
					<div class="form-row">
						<input name="id" type="hidden" value="<?php echo $id ?>" />
						<input name="editedDate" type="hidden" value="<?php echo $editedDate ?>" />
						<input class="input-btn" type="submit" name="edit" value="Update" />
						<input class="input-btn btn-cancel" type="button" name="cancel" value="Cancel" onclick="history.go(-1)" />
					</div>
				</form>
				<br />
			</div>
		</div>
		<?php
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>