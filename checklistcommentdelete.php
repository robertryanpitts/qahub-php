<?php
	require_once('includes/config.php');
	require_once('authorize.php');
	$sth = $dbh->prepare("DELETE FROM comments WHERE commentId = ?");
	$sth->execute(array($_GET['commentId']));
	$editedDate = date('Y-m-d  H:i:s');
	$sth = $dbh->prepare("UPDATE releases SET modifiedBy = ?, editedDate = ? WHERE id = ?");
	$sth->execute(array($_COOKIE['user'], $editedDate, $_GET['releaseId']));
	require_once('includes/closeconn.php');
	$arr = array ('tableName' => $_GET['tableName'], 'releaseId' => $_GET['releaseId'], 'rowId' => $_GET['rowId'], 'commentId' => $_GET['commentId'], 'editedDate' => date('F j, Y @ g:ia', strtotime($editedDate)), 'editedBy' => $_COOKIE['editedBy']);
	echo json_encode($arr);
?>