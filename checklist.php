<?php
	require_once('includes/config.php');
	require_once('authorize.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
		require_once('includes/meta.php');
		$sth = $dbh->prepare("SELECT * FROM releases WHERE id = ?");
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$sth->execute(array($_GET['releaseId']));
		while($row = $sth->fetch()){
			$name = $row->name;
			$owner = $row->owner;
			$backupContact = $row->backupContact;
			$type = $row->type;
			($row->dateDeploy == '' || $row->dateDeploy == '0000-00-00 00:00:00') ? $dateDeploy = 'Date not set.' : $dateDeploy = date('F j, Y', strtotime($row->dateDeploy));
			($row->dateCodeCutoff == '' || $row->dateCodeCutoff == '0000-00-00 00:00:00') ? $dateCodeCutoff = 'Date not set.' : $dateCodeCutoff = date('F j, Y', strtotime($row->dateCodeCutoff));
		}
	?>
	<script type="text/javascript">
		var globalCommentType = '';
		var globalTableName = '';
		var globalRowId = '';
		var globalCommentId = '';

		$(document).ready(function(){
			$('.error-handling').fancybox({
				autoSize: false,
				height: 225,
				scrolling: 'no',
				width: 500,
				type: 'iframe',
				padding: 0
			});
			$('.flex-urls').fancybox({
				autoSize: false,
				height: 285,
				width: 550,
				scrolling: 'no',
				type: 'iframe',
				padding: 0
			});
			$('.comment-box-popup').fancybox({
				autoSize: false,
				height: 500,
				width: 675,
				scrolling: 'no',
				type: 'iframe',
				padding: 0,
				afterClose: function(){
					if(globalCommentType == 'Add'){
						$.ajax({
							type: 'GET',
							url: 'checklistgetcommentdata.php?commentType=Add',
							success: function(data){
								if(data.editedDate != 0){
									$('#' + globalTableName + ' div[class="comment-box-add' + globalRowId + '"]').next('ul').append('<li style="display:none;" class="comment-box-edit' + data.rowId + '" comment="' + data.commentId + '"><div class="bullet-arrow left"></div>Comment By: <strong>' + data.editedBy + '</strong> <span class="checklist-edited-data">(last updated on ' + data.editedDate + ')</span>&nbsp;&nbsp;<a class="comment-box-popup" href="checklistcommentedit.php?releaseId=' + data.releaseId + '&tableName=' + data.tableName + '&rowId=' + data.rowId + '&commentId=' + data.commentId + '" onclick="globalCommentType = \'Edit\'; globalTableName = \'' + data.tableName + '\'; globalRowId = \'' + data.rowId + '\'; globalCommentId = \'' + data.commentId + '\';"><img src="images/iconEdit.png" title="Edit Comment" /></a>&nbsp;&nbsp;<a href="#" onclick="if(confirm(\'Are you sure that you want to delete this comment?\')){ commentDelete(' + data.releaseId + ',\'' + data.tableName + '\',' + data.rowId + ',' + data.commentId + '); return false; }else{ return false; }"><img src="images/iconDelete.png" title="Delete Comment" /></a><span class="comments-data">' + data.comment + '</span></li>');
									$('li[class="comment-box-edit' + data.rowId + '"][comment="' + data.commentId + '"]').fadeIn(250);
									if(globalTableName != 'checklistproject'){
										$('#' + data.tableName + ' div[class="row' + data.rowId + '"] .checklist-edited-data:first').fadeOut(250, function(){
											$(this).html(' - <span class="checklist-edit-date">(last updated on <span class="checklist-new-edited-date">' + data.editedDate + '</span> by <span class="checklist-new-edited-by">' + data.editedBy + '</span>)</span></span>').fadeIn(250);
										});
									}
								}else{
									// do nothing
								}
							},
							error: function(){
								alert('There was an error getting data from the database.  Please contact your administrator to have this issue resolved.');
							},
							dataType: 'json'
						});
					}else if(globalCommentType == 'Edit'){
						$.ajax({
							type: 'GET',
							url: 'checklistgetcommentdata.php?commentType=Edit&commentId=' + globalCommentId,
							success: function(data){
								$('li[class="comment-box-edit' + data.rowId + '"][comment="' + data.commentId + '"]').fadeOut(250, function(){
									$(this).html('<div class="bullet-arrow left"></div>Comment By: <strong>' + data.editedBy + '</strong> <span class="checklist-edited-data">(last updated on ' + data.editedDate + ')</span>&nbsp;&nbsp;<a class="comment-box-popup" href="checklistcommentedit.php?releaseId=' + data.releaseId + '&tableName=' + data.tableName + '&rowId=' + data.rowId + '&commentId=' + data.commentId + '" onclick="globalCommentType = \'Edit\'; globalTableName = \'' + data.tableName + '\'; globalRowId = \'' + data.rowId + '\'; globalCommentId = \'' + data.commentId + '\';"><img src="images/iconEdit.png" title="Edit Comment" /></a>&nbsp;&nbsp;<a href="#" onclick="if(confirm(\'Are you sure that you want to delete this comment?\')){ commentDelete(' + data.releaseId + ',\'' + data.tableName + '\',' + data.rowId + ',' + data.commentId + '); return false; }else{ return false; }"><img src="images/iconDelete.png" title="Delete Comment" /></a><span class="comments-data">' + data.comment + '</span>').fadeIn(250);
								});
								if(globalTableName != 'checklistproject'){
									$('#' + data.tableName + ' div[class="row' + data.rowId + '"] .checklist-edited-data:first .checklist-new-edited-date').fadeOut(250, function(){
										$(this).text(data.editedDate).fadeIn(250);
									});
									$('#' + data.tableName + ' div[class="row' + data.rowId + '"] .checklist-edited-data:first .checklist-new-edited-by').fadeOut(250, function(){
										$(this).html(data.editedBy).fadeIn(250);
									});
								}
							},
							error: function(){
								alert('There was an error getting data from the database.  Please contact your administrator to have this issue resolved.');
							},
							dataType: 'json'
						});
					}
				}
			});
			
			markCompleted();
		});

		var markCompleted = function markCompleted(){
			if($('#checklistrelease').find('input[type=checkbox]:not(:checked)').length == 0){
				$('#checklistrelease').find('h2 .completed').css('visibility','visible');
			}else{
				$('#checklistrelease').find('h2 .completed').css('visibility','hidden');
			}
			
			if($('#checklistdeploy').find('input[type=checkbox]:not(:checked)').length == 0){
				$('#checklistdeploy').find('h2 .completed').css('visibility','visible');
			}else{
				$('#checklistdeploy').find('h2 .completed').css('visibility','hidden');
			}
			
			if($('#checklistproduction').find('input[type=checkbox]:not(:checked)').length == 0){
				$('#checklistproduction').find('h2 .completed').css('visibility','visible');
			}else{
				$('#checklistproduction').find('h2 .completed').css('visibility','hidden');
			}
		};
	</script>
	<title>QA Hub | Admin | <?php echo $name; ?> Checklist</title>
</head>
<body>
	<div id="content-wrapper">
		<?php
			require_once('header.php');
		?>
		<div id="main-wrapper">
			<?php
				require_once('menunav.php');
			?>
			<div id="main-content">
				<?php
					echo '<h1 class="left">Checklist for ' . $name . '</h1>';
					echo '<h2 class="right"><a href="releaseitems.php?releaseId=' . $_GET['releaseId'] . '">View Release List for ' . $name . '</a></h2>';
					echo '<div class="checklist-details left">';
						echo '<div class="left">Owner: <span class="accent">' . $owner . '</span></div>';
						echo '<br />';
						echo '<div class="left">Backup Contact: <span class="accent">' . $backupContact . '</span></div>';
						echo '<br />';
						echo '<div class="left">Release Date: <span class="accent">' . $dateDeploy . '</span></div>';
						echo '<br />';
						echo '<div class="left">Code Cutoff Date: <span class="accent">' . $dateCodeCutoff . '</span></div>';
					echo '</div>';
					//table data - tools
					echo '<div id="tools" class="left">';
						$sth = $dbh->prepare("SELECT * FROM tools WHERE releaseId = ? AND active = 'true' ORDER BY editedDate DESC LIMIT 1");
						$sth->setFetchMode(PDO::FETCH_OBJ);
						$sth->execute(array($_GET['releaseId']));
						while($row = $sth->fetch()){
							($row->editedDate == '0000-00-00 00:00:00') ? $toolsDateEditedDate = '' : $toolsDateEditedDate = date('F j, Y @ g:ia', strtotime($row->editedDate));
							echo ($toolsDateEditedDate == '') ? '<h2>Products Affected <span class="checklist-edited-data"></span></h2>' : '<h2>Products Affected <span class="checklist-edited-data"> - <span class="checklist-edit-date">(last updated on <span class="checklist-new-edited-date">' . $toolsDateEditedDate . '</span> by <span class="checklist-new-edited-by">' . $row->editedBy . '</span>)</span></span></span></h2>';
						}
						$sth = $dbh->prepare("SELECT * FROM tools WHERE releaseId = ? AND active = 'true' ORDER BY rowId ASC");
						$sth->setFetchMode(PDO::FETCH_OBJ);
						$sth->execute(array($_GET['releaseId']));
						while($row = $sth->fetch()){
							echo '<img src="images/' . $row->label . '_' . $row->value . '.png" class="tool" release="' . $row->releaseId . '" table="tools" label="' . $row->label . '" value="' . $row->value . '" onclick="submitDataTools($(this))" width="85" /></a>';
						}
					echo '</div>';
					// data for release, stage, and production checklists
					$checklists = array('Release' => 'checklistrelease', 'Stage' => 'checklistdeploy', 'Production' => 'checklistproduction');
					foreach($checklists as $k => $v){
						echo '<div id="' . $v . '" class="left">';
							echo '<h2>' . $k . ' Branch (integrated)<span class="completed"> - <span class="success-message">Completed</span></span></h2>';
							$sql = "SELECT * FROM " . $v . " WHERE releaseId = ? AND active = 'true' ORDER BY rowId ASC";
							$sth = $dbh->prepare($sql);
							$sth->setFetchMode(PDO::FETCH_OBJ);
							$sth->execute(array($_GET['releaseId']));
							while($row = $sth->fetch()){
								$rowId = $row->rowId;
								($row->value == 'true') ? $releaseValueCheck = 'checked="checked"' : $releaseValueCheck = '';
								($row->ditedDate == '0000-00-00 00:00:00') ? $releaseEditedDate = '' : $releaseEditedDate = date('F j, Y @ g:ia', strtotime($row->editedDate));
								echo '<div class="checklist-row row' . $row->rowId . '">';
									if($row->applicable == 'false'){
										$titleAndCheckbox = '<strong><span class="strike">' . $row->label . '</span></strong>';
										$markApplicable = '<a href="#" onclick="applicableTrue(' . $_GET['releaseId'] . ',\'' . $v . '\',' . $row->rowId . '); return false;"><img src="images/iconSave.png" title="Mark Applicable" /> Mark Applicable</a>';
									}else if($row->applicable == 'true'){
										$titleAndCheckbox = '<input class="' . $row->rowId . '" release="' . $_GET['releaseId'] . '" table="' . $v . '" rowId="' . $rowId . '" type="checkbox" ' . $releaseValueCheck . ' onchange="submitDataCheckbox($(this))" /> <strong>' . $row->label . '</strong>';
										$markApplicable = '<a href="#" onclick="applicableFalse(' . $_GET['releaseId'] . ',\'' . $v . '\',' . $row->rowId . '); return false;"><img src="images/iconNotApplicable.png" title="Mark Not Applicable" /> Mark Not Applicable</a>';
									}
									echo '<label>' . $titleAndCheckbox . '<span class="checklist-row-edited-data">';
										echo ($row->editedBy) ? ' - <span class="checklist-edit-date">(last updated on <span class="checklist-new-edited-date">' . $row->editedDate . '</span> by <span class="checklist-new-edited-by">' . $row->editedBy . '</span>)</span>' : '';
									echo '</span></label>';
									echo '<div class="comment-box-add' . $row->rowId . '" comment="0">' . $markApplicable . '&nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;<a class="comment-box-popup" href="checklistcommentadd.php?releaseId=' . $_GET['releaseId'] . '&tableName=' . $v . '&rowId=' . $row->rowId . '" onclick="globalCommentType = \'Add\'; globalTableName = \'' . $v . '\'; globalRowId = \'' . $row->rowId . '\';"><img src="images/iconAdd.png" title="Add A Comment" /> Add A Comment</a></div>';
									echo '<ul class="comments-list">';
										$sthComment = $dbh->prepare("SELECT * FROM comments WHERE releaseId = ? AND tableName = ? AND rowId = ? ORDER BY commentId ASC");
										$sthComment->setFetchMode(PDO::FETCH_OBJ);
										$sthComment->execute(array($_GET['releaseId'], $v, $row->rowId));
										while($rowComment = $sthComment->fetch()){
											if($rowComment->comment){
												echo '<li class="comment-box-edit' . $rowComment->rowId . '" comment="' . $rowComment->commentId . '"><div class="bullet-arrow left"></div>Comment By: <strong>' . $rowComment->editedBy . '</strong> <span class="checklist-edited-data">(last updated on ' . date('F j, Y @ g:ia', strtotime($rowComment->editedDate)) . ')</span>&nbsp;&nbsp;<a class="comment-box-popup" href="checklistcommentedit.php?releaseId='. $rowComment->releaseId .'&tableName=' . $rowComment->tableName . '&rowId=' . $rowComment->rowId . '&commentId='. $rowComment->commentId .'" onclick="globalCommentType = \'Edit\'; globalTableName = \'' . $rowComment->tableName . '\'; globalRowId = \'' . $rowComment->rowId . '\'; globalCommentId = \'' . $rowComment->commentId . '\';"><img src="images/iconEdit.png" title="Edit Comment" /></a>&nbsp;&nbsp;<a href="#" onclick="if(confirm(\'Are you sure that you want to delete this comment?\')){ commentDelete(' . $rowComment->releaseId . ',\'' . $rowComment->tableName . '\',' . $rowComment->rowId . ',' . $rowComment->commentId . '); return false; }else{ return false; }"><img src="images/iconDelete.png" title="Delete Comment" /></a><span class="comments-data">' . $rowComment->comment . '</span></li>';
											}
										}
									echo '</ul>';
								echo '</div>';
							}
						echo '</div>';
					}
					// data for projects
					$sth = $dbh->prepare("SELECT * FROM releasespatches WHERE releaseId = ? AND type = 'Project' ORDER BY itemId ASC");
					$sth->setFetchMode(PDO::FETCH_OBJ);
					$sth->execute(array($_GET['releaseId']));
					if($sth->rowCount() > 0){
						echo '<div id="checklistproject" class="left">';
							echo '<h2>Notes for Projects Being Released</h2>';
							while($row = $sth->fetch()){
								($projectEditedDate == '0000-00-00 00:00:00') ? $projectEditedDate = '' : $projectEditedDate = date('F j, Y @ g:ia', strtotime($row->editedDate));
								echo '<div class="checklist-row row' . $row->itemId . '">';
									echo '<strong>' . $row->title . '</strong>';
									echo '<div class="comment-box-add' . $row->itemId . '" comment="0"><a href="#" onclick="applicableFalse(' . $_GET['releaseId'] . ',\'checklistproject\',' . $row->itemId . '); return false;"><img src="images/iconNotApplicable.png" title="Mark Not Applicable" /> Mark Not Applicable</a>&nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;<a class="comment-box-popup" href="checklistcommentadd.php?releaseId=' . $_GET['releaseId'] . '&tableName=checklistproject&rowId=' . $row->itemId . '" onclick="globalCommentType = \'Add\'; globalTableName = \'checklistproject\'; globalRowId = \'' . $row->itemId . '\';"><img src="images/iconAdd.png" title="Add A Comment" /> Add A Comment</a></div>';
									echo '<ul class="comments-list">';
										$sthComment = $dbh->prepare("SELECT * FROM comments WHERE tableName = ? AND releaseId = ? AND rowId = ? ORDER BY commentId ASC");
										$sthComment->setFetchMode(PDO::FETCH_OBJ);
										$sthComment->execute(array('checklistproject', $_GET['releaseId'], $row->itemId));
										while($rowComment = $sthComment->fetch()){
											$commentIdProject = $projectComment['commentId'];
											$releaseIdProjectComment = $projectComment['releaseId'];
											$tableNameProjectComment = $projectComment['tableName'];
											$rowIdProjectComment = $projectComment['rowId'];
											$commentProject = $projectComment['comment'];
											$editedByProjectComment = $projectComment['editedBy'];
											$editedDateProjectComment = date('F j, Y @ g:ia', strtotime($projectComment['editedDate']));
											
											if($rowComment->comment){
												echo '<li class="comment-box-edit' . $rowComment->rowId . '" comment="' . $rowComment->commentId . '"><div class="bullet-arrow left"></div>Comment By: <strong>' . $rowComment->editedBy . '</strong> <span class="checklist-edited-data">(last updated on ' . date('F j, Y @ g:ia', strtotime($rowComment->editedDate)) . ')</span>&nbsp;&nbsp;<a class="comment-box-popup" href="checklistcommentedit.php?releaseId='. $rowComment->releaseId .'&tableName=' . $rowComment->tableName . '&rowId=' . $rowComment->rowId . '&commentId='. $rowComment->commentId .'" onclick="globalCommentType = \'Edit\'; globalTableName = \'' . $rowComment->tableName . '\'; globalRowId = \'' . $rowComment->rowId . '\'; globalCommentId = \'' . $rowComment->commentId . '\';"><img src="images/iconEdit.png" title="Edit Comment" /></a>&nbsp;&nbsp;<a href="#" onclick="if(confirm(\'Are you sure that you want to delete this comment?\')){ commentDelete(' . $rowComment->releaseId . ',\'' . $rowComment->tableName . '\',' . $rowComment->rowId . ',' . $rowComment->commentId . '); return false; }else{ return false; }"><img src="images/iconDelete.png" title="Delete Comment" /></a><span class="comments-data">' . $rowComment->comment . '</span></li>';
											}
										}
									echo '</ul>';
								echo '</div>';
							}
						echo '</div>';
					}
					echo '<div class="clear"></div>';
				?>
			</div>
		</div>
		<?php
			require_once('footer.php');
			require_once('includes/closeconn.php');
		?>
	</div>
</body>
</html>