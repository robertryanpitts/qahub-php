<?php
	require_once('includes/config.php');
	require_once('authorize.php');

	if(isset($_POST['add'])){
		// Adding a record to the "releases" table
		$sth = $dbh->prepare("INSERT INTO releases (id, name, owner, backupContact, dateCreated, editedDate, dateDeploy, dateCodeCutoff, active) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$sth->execute(array($_POST['id'], $_POST['name'], $_POST['owner'], $_POST['backupContact'], $_POST['dateCreated'], '0000-00-00 00:00:00', str_replace('/', '-', $_POST['dateDeploy']), str_replace('/', '-', $_POST['dateCodeCutoff']), $_POST['active']));

		// initializing and filling arrays to create bulk insert statements
		$sth = $dbh->prepare("SELECT section,label FROM releasesdata ORDER BY section ASC, priority ASC");
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$sth->execute();
		$releaseLabels = array();
		$deployLabels = array();
		$productionLabels = array();
		$toolsLabels = array();
		while($row = $sth->fetch()){
			if($row->section == 'checklistrelease'){
				$releaseLabels[] = $row->label;
			}else if($row->section == 'checklistdeploy'){
				$deployLabels[] = $row->label;
			}else if($row->section == 'checklistproduction'){
				$productionLabels[] = $row->label;
			}else if($row->section == 'tools'){
				$toolsLabels[] = $row->label;
			}
		}
		$labels = array('checklistrelease' => $releaseLabels, 'checklistdeploy' => $deployLabels, 'checklistproduction' => $productionLabels);
		
		// Adding records to the "checklistrelease", "checklistdeploy", and "checklistproduction" table
		foreach($labels as $k => $v){
			$sql = "INSERT INTO ".$k." (releaseId, label, value, applicable, active, editedDate) VALUES ";
			foreach($v as $key => $val){
				$sql .= "('".$_POST['id']."', '".$val."', 'false', 'true', 'true', '0000-00-00 00:00:00'),";
			}
			$sql = substr($sql, 0, -1);
			$sth = $dbh->prepare($sql);
			$sth->execute();
		}

		// Adding records to the "tools" table
		$sql = "INSERT INTO tools (releaseId, label, value, active, editedBy, editedDate) VALUES ";
		foreach($toolsLabels as $k => $v){
			$sql .= "('".$_POST['id']."', '".$v."', 'false', 'true', '', '0000-00-00 00:00:00'),";
		}
		$sql = substr($sql, 0, -1);
		$sth = $dbh->prepare($sql);
		$sth->execute();
		echo '<br /><br /><br />';
	}
	require_once('includes/closeconn.php');
	header('location:releases.php?add=success');
?>