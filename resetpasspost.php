<?php
	require_once('includes/config.php');

	if(isset($_POST['reset_pass'])){
		if(empty($_POST['BotCatcher'])){
			if($_POST['firstTimeUser'] == 'true'){
				$sth = $dbh->prepare("UPDATE users SET password = ?, editedDate = ?, dateLastLoggedIn = ? WHERE username = ?");
				$sth->execute(array(md5($_POST['password']), $_POST['editedDate'], $_POST['editedDate'], $_POST['username']));
				$sthAdmin = $dbh->prepare("SELECT * from users WHERE username = ?");
				$sthAdmin->setFetchMode(PDO::FETCH_OBJ);
				$sthAdmin->execute(array($_POST['username']));
				while($row = $sthAdmin->fetch()){
					$selectedColorScheme = $row->selectedColorScheme;
					if($row->isAdmin == 'true'){
						setcookie('admin','true',time()+60*60*24*365);
					}
				}
				if($_POST['remember'] == 'true'){
					setcookie('user',$_POST['username'],time()+60*60*24*365);
				}else{
					setcookie('user',$_POST['username'],time()+60*60*4);
				}
				require_once('includes/closeconn.php');
				header('location:home.php');
			}else{
				$sth = $dbh->prepare("UPDATE users SET password = ?, editedDate = ? WHERE username = ?");
				$sth->execute(array(md5($_POST['password']), $_POST['editedDate'], $_POST['username']));
				require_once('includes/closeconn.php');
				header('location:resetpass.php?reset=success');
			}
		}
	}
?>